import xmltodict
from collections import OrderedDict
from keystoneclient.auth.identity import v3
from keystoneauth1 import session
import keystoneauth1.exceptions.http
from keystoneclient.v3 import client
from datetime import datetime


class Packet:
    def __init__(self, **kwargs):
        if not "xml" in kwargs:
            self.xml = OrderedDict()
            self.xml["amie"] = OrderedDict(
                [
                    ("@version", "1.0"),
                ]
            )

            header = self.gen_header(**kwargs)
            body = OrderedDict()
            if kwargs["type"] == "request_project_create":
                body = self.gen_rpc_body(body, **kwargs)
            elif kwargs["type"] == "inform_transaction_complete":
                body = self.gen_itc_body(body, **kwargs)
            elif kwargs["type"] == "notify_project_create":
                body = self.gen_npc_body(body, **kwargs)
            elif kwargs["type"] == "data_project_create":
                body = self.gen_dpc_body(body, **kwargs)
            elif kwargs["type"] == "request_account_create":
                body = self.gen_rac_body(body, **kwargs)
            elif kwargs["type"] == "notify_account_create":
                body = self.gen_nac_body(body, **kwargs)
            elif kwargs["type"] == "data_account_create":
                body = self.gen_dac_body(body, **kwargs)
            elif kwargs["type"] == "notify_project_usage":
                body = self.gen_npu_body(body, **kwargs)
            self.xml["amie"][kwargs["type"]] = OrderedDict(
                [("header", header), ("body", body)]
            )
        else:
            if "npc_from_rpc" in kwargs and kwargs["npc_from_rpc"]:
                self.xml = OrderedDict()
                self.xml["amie"] = OrderedDict(
                    [
                        ("@version", "1.0"),
                    ]
                )
                previous_header = kwargs["xml"]["amie"]["request_project_create"][
                    "header"
                ]
                self.xml["amie"]["notify_project_create"] = OrderedDict(
                    [
                        (
                            "header",
                            gen_reply_header(self, previous_header=previous_header),
                        ),
                    ]
                )
                self.xml["amie"]["notify_project_create"]["body"] = gen_reply_npc(
                    self, **kwargs
                )
            elif "nac_from_rac" in kwargs and kwargs["nac_from_rac"]:
                self.xml = OrderedDict()
                self.xml["amie"] = OrderedDict(
                    [
                        ("@version", "1.0"),
                    ]
                )
                previous_header = kwargs["xml"]["amie"]["request_account_create"][
                    "header"
                ]
                self.xml["amie"]["notify_account_create"] = OrderedDict(
                    [
                        (
                            "header",
                            gen_reply_header(self, previous_header=previous_header),
                        ),
                    ]
                )
                self.xml["amie"]["notify_account_create"]["body"] = gen_reply_nac(
                    self, **kwargs
                )
            else:
                self.xml = xml
            return

    def gen_header(self, **kwargs):
        header = OrderedDict()
        header["date"] = kwargs["date"]
        header["from_site_name"] = kwargs["from_site_name"]
        header["originating_site_name"] = kwargs["originating_site_name"]
        header["packet_id"] = kwargs["packet_id"]
        header["to_site_name"] = kwargs["to_site_name"]
        header["transaction_id"] = kwargs["transaction_id"]
        return header

    def gen_reply_header(self, **kwargs):
        header = OrderedDict()
        header["date"] = datetime.now()
        header["from_site_name"] = previous_header["to_site"]
        header["originating_site_name"] = previous_header["originating_site_name"]
        header["packet_id"] = int(previous_header["packet_id"]) + 1
        header["to_site_name"] = previous_header["from_site"]
        header["transaction_id"] = previous_header["transaction_id"]
        return header

    def gen_reply_npc(self, **kwargs):
        pass

    def gen_reply_nac(self, **kwargs):
        pass

    def gen_rpc_body(self, body, **kwargs):
        body["alloc_type"] = kwargs["alloc_type"]
        body["end_date"] = kwargs["end_date"]
        body["grant_num"] = kwargs["grant_num"]
        body["pfos"] = {"number": kwargs["pfos"]}
        body["pi"] = OrderedDict()
        body["pi"]["personal_info"] = OrderedDict()
        body["pi"]["personal_info"]["first_name"] = kwargs["first_name"]
        body["pi"]["personal_info"]["last_name"] = kwargs["last_name"]
        body["pi"]["personal_info"]["organization"] = kwargs["organization"]
        body["pi"]["personal_info"]["org_code"] = kwargs["org_code"]
        # drill down on pi here
        body["resource_list"] = {"resource": kwargs["resource_list"]}

        body["su_alloc"] = kwargs["su_alloc"]
        body["start_date"] = kwargs["start_date"]
        return body

    def gen_itc_body(self, body, **kwargs):
        body["detail_code"] = kwargs["detail_code"]
        body["message"] = kwargs["message"]
        body["status_code"] = kwargs["status_code"]
        return body

    def gen_npc_body(self, body, **kwargs):
        body["grant_num"] = kwargs["grant_num"]
        body["pfos"] = {"number": kwargs["pfos"]}
        body["pi"] = OrderedDict()
        body["pi"]["personal_info"] = OrderedDict()
        body["pi"]["personal_info"]["org_code"] = kwargs["org_code"]
        body["pi"]["personal_info"]["person_id"] = kwargs["person_id"]
        body["pi"]["remote_site_login"] = kwargs["remote_site_login"]
        # drill down on pi here
        body["project_id"] = kwargs["project_id"]
        body["project_title"] = kwargs["project_title"]
        body["resource_list"] = {"resource": kwargs["resource_list"]}

        body["su_alloc"] = kwargs["su_alloc"]
        body["start_date"] = kwargs["start_date"]
        return body

    def gen_dpc_body(self, body, **kwargs):
        body["person_id"] = kwargs["person_id"]
        body["project_id"] = kwargs["project_id"]
        return body

    def gen_rac_body(self, body, **kwargs):
        body["grant_num"] = kwargs["grant_num"]
        body["resource_list"] = {"resource": kwargs["resource_list"]}
        body["user"] = OrderedDict()
        return body

    def gen_nac_body(self, body, **kwargs):
        body["project_id"] = kwargs["project_id"]
        body["resource_list"] = {"resource": kwargs["resource_list"]}
        body["user"] = OrderedDict()
        return body

    def gen_dac_body(self, body, **kwargs):
        return body

    def gen_npu_body(self, body, **kwargs):
        body["usage_type"] = kwargs["usage_type"]
        body["project_id"] = kwargs["project_id"]
        body["machine_name"] = kwargs["machine_name"]
        body["charge"] = kwargs["charge"]
        body["end_time"] = kwargs["end_time"]
        body["record_identity"] = kwargs["record_id"]
        body["start_time"] = kwargs["start_time"]
        body["submit_time"] = kwargs["submit_time"]
        body["user_login"] = kwargs["user_login"]
        body["wall_duration"] = kwargs["wall_duration"]
        return body


class Process:
    def __init__(self, **kwargs):
        self.conf = conf
        self.auth = v3.Password(
            auth_url=conf["os_auth_url"],
            user_id=conf["os_user_id"],
            password=conf["os_password"],
            project_id=conf["os_project_id"],
        )
        self.sess = session.Session(auth=self.auth)
        self.keystone = client.Client(session=self.sess)

    def process_rpc(self, transaction):
        # persist project meta data here
        persist_project(self, packet)
        project = self.keystone.projects.create(
            name=project_name, domain=self.conf["domain"]
        )
        group = self.keystone.groups.create(
            name=project_name, domain=self.conf["domain"]
        )
        # lookup pi, if they don't exist create new user
        user = self.keystone.users.list(domain=self.conf["domain"], name=username)
        if len(user) == 0:
            user = self.keystone.users.create(
                user_name, domain=self.conf["domain"], email=email, description=fullname
            )
        self.keystone.users.add_to_group(user.id, group.id)
        # create response here

    def process_rac(self, transaction):
        # persist user metadata here
        persist_user(self, packet)
        group = self.keystone.groups.list(domain=conf["domain"], name=project)[0]
        user = self.keystone.projects.create(
            user_name, domain=self.conf["domain"], email=email, description=fullname
        )
        self.keystone.users.add_to_group(user.id, group.id)
        # user created in keystone, generate reply

    def persist_project(self, transaction):
        pass

    def persist_user(self, transaction):
        pass
