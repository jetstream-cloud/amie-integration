from configparser import ConfigParser
from amieclient import AMIEClient
from amieclient.packet import NotifyPersonIDs

# from amie import util
import util
import json
import requests
import sys
import uuid


class PacketProcessor:
    def __init__(self, testing=False):
        config = ConfigParser()
        config.read("config.ini")
        if "IU" in config:
            site_config = config["IU"]

        else:
            site_config = {}
            site_config["site_name"] = "testsite"
            site_config["amie_url"] = "https://example.com"
            site_config["api_key"] = "secretkey"
        self.amie_client = AMIEClient(
            site_name=site_config["site_name"],
            amie_url=site_config["amie_url"],
            api_key=site_config["api_key"],
        )
        self.u = util.Util(testing, site_config["db_connection"], conf=site_config)
        self.slack_webhook_url = site_config["slack_webhook_url"]
        self.jta_project_name = site_config["jta_project_name"]
        # self.packets = self.amie_client.list_packets().packets

    def packet_filter(self, packets, packet_filter={}):
        """Filter list of packets based on dict with type, project, and resoruce"""
        if "type" in packet_filter:
            packets = [x for x in packets if x.packet_type == packet_filter["type"]]
        if "project" in packet_filter:
            plist = []
            for packet in packets:
                if (
                    hasattr(packet, "ChargeNumber")
                    and packet.ChargeNumber == packet_filter["project"]
                ):
                    plist.append(packet)
                elif (
                    hasattr(packet, "ProjectID")
                    and packet.ProjectID == packet_filter["project"]
                ):
                    plist.append(packet)
            packets = plist
        if "resource" in packet_filter:
            packets = [
                x for x in packets if x.AllocatedResource == packet_filter["resource"]
            ]
        return packets

    def process_packets(self, packets):
        """Log packet contents then select action based on packet type"""

        for packet in packets:
            packet_dict = packet.as_dict()
            packet_dict["header"]["packet_timestamp"] = packet_dict["header"][
                "packet_timestamp"
            ].isoformat()
            packetlog = util.model.PacketLog(
                transaction_id=packet.transaction_id,
                packet_id=packet.packet_id,
                packet_timestamp=packet.packet_timestamp,
                data=packet_dict,
            )
            self.u.session.add(packetlog)
            self.u.session.commit()
            if packet.packet_type == "request_project_create":
                self.process_rpc(packet)
            elif packet.packet_type == "request_account_create":
                self.process_rac(packet)
            elif (
                packet.packet_type == "data_project_create"
                or packet.packet_type == "data_account_create"
            ):
                self.process_dpc_dac(packet)
            elif packet.packet_type == "request_user_modify":
                self.process_rum(packet)
            elif packet.packet_type == "request_person_merge":
                self.process_rpm(packet)
            elif packet.packet_type == "request_project_inactivate":
                self.process_rpi(packet)
            elif packet.packet_type == "request_account_inactivate":
                self.process_rai(packet)
            elif packet.packet_type == "request_project_reactivate":
                self.process_rpr(packet)
            elif packet.packet_type == "inform_transaction_complete":
                self.process_itc(packet)

    def create_npi_update_personid(self, old_personid, new_personid):
        npi = NotifyPersonIDs()
        npi.transaction_id = str(uuid.uuid1())
        npi.originating_site_name = "IU"
        npi.PersonIDList = [old_personid, new_personid]
        npi.PersonID = old_personid
        npi.PrimaryPersonID = new_personid
        self.amie_client.send_packet(npi)

    def create_itc(self, packet):
        """Given a packet create a Inform Transaction Complete reply packet"""

        itc = packet.reply_packet()
        itc.StatusCode = "Success"
        itc.DetailCode = "1"
        itc.Message = "OK"
        return itc

    def process_itc(self, packet):
        itc = self.create_itc(packet)
        self.amie_client.send_packet(itc)

    def process_dpc_dac(self, packet):
        """Given a Data Project Create packet containing the a list of the PIs DNs send an ITC
        because we do not use or track users DNs"""

        itc = self.create_itc(packet)
        self.amie_client.send_packet(itc)

    def process_rac(self, packet):
        """Given a Request Account Create packet, find the assigned user name if there is one if not create
        an openstack user using the xsede portal user name
        ###TODO### move requested_login to use the xsede portal name, check person_id is login or uuid
        ###TODO### reactivate is implicit"""
        #        if :
        #          project_id = packet.ProjectID.replace('TG-','')  # site project_id
        #        else:
        project_id = packet.GrantNumber.replace("TG-", "")
        resource = (
            packet.AllocatedResource
        )  # xsede site resource name, eg, delta.ncsa.xsede.org
        if not resource:
            resource = packet.ResourceList[0]
        user_login = [
            x["PersonID"] for x in packet.SitePersonId if x["Site"] == "X-PORTAL"
        ][0]
        #        user_person_id = packet.UserPersonID         # site person_id for the User (if known)
        #        user_login = packet.UserRemoteSiteLogin  # login on resource for the User (if known)
        user_email = packet.UserEmail
        user_person_id = self.u.create_openstack_account(user_login, user_email)
        if project_id != self.jta_project_name:
            self.u.user_group_add(project_id, user_person_id)
        else:
            self.u.create_openstack_project(
                user_login,
                "JTA User",
                [],
                resource,
                board_type="jta",
                award=1,
                allocation_type=None,
                jta=True,
            )
            self.u.user_group_add(user_login, user_person_id)
        nac = packet.reply_packet()
        nac.ProjectID = project_id  # local project ID
        nac.UserRemoteSiteLogin = user_login  # local login for the User on the resource
        nac.UserPersonID = user_person_id  # local person ID for the User
        #        print(nac.as_dict())
        # send the NAC
        self.amie_client.send_packet(nac)

    def process_rum(self, packet):
        """Given a Request User Modify if the action is not delete update the user's email and
        reply with an ITC, we can only replace an email address not delete it"""

        person_id = packet.PersonID

        if packet.ActionType != "delete":
            email = packet.Email
            self.u.update_email(person_id, email)

        itc = self.create_itc(packet)
        self.amie_client.send_packet(itc)

    def process_rpm(self, packet):
        """NOOP ITC reply"""
        itc = self.create_itc(packet)
        self.amie_client.send_packet(itc)

    def process_rpi(self, packet):
        """Disable openstack project and reply with Notify Project Inactivate"""
        self.u.disable_project(
            packet.ProjectID.replace("TG-", ""),
            packet.AllocatedResource.lower(),
            packet.StartDate,
            packet.EndDate,
        )
        npi = packet.reply_packet()
        self.amie_client.send_packet(npi)

    def process_rai(self, packet):
        """Request Account Inactivate, remove user from group belonging to the project and
        reply with a Notify Account Inactivate"""
        project_id = packet.ProjectID.replace("TG-", "")
        person_id = packet.PersonID

        self.u.user_group_remove(person_id, project_id)

        nai = packet.reply_packet()
        self.amie_client.send_packet(nai)

    def process_rpr(self, packet):
        """Set the openstack project active again having received a Request Project Reactivate
        and reply with a Notify Project Reactivate"""

        self.u.enable_project(packet.ProjectID.replace("TG-", ""))
        npr = packet.reply_packet()
        self.amie_client.send_packet(npr)

    def process_rpc(self, packet):
        """Given a Request Project Create, create the openstack project, create group, add role for group on project
        create openstack user for PI, add user to group, record project information in database, reply with
        Notify Project Create"""

        charge_code = packet.ChargeNumber.replace("TG-", "")
        if packet.ProjectID:
            project_id = packet.ProjectID.replace(
                "TG-", ""
            )  # site project_id (if known)
        else:
            project_id = charge_code
        resource_list = (
            packet.ResourceList
        )  # xsede site resource name, eg, delta.ncsa.xsede.org
        allocation_type = (
            packet.AllocationType
        )  # new, renewal, supplement, transfer, adjustment, advance, extension, ...
        start_date = packet.StartDate
        end_date = packet.EndDate
        amount = packet.ServiceUnitsAllocated
        abstract = packet.Abstract
        project_title = packet.ProjectTitle
        pfos_num = packet.PfosNumber
        pi_global_id = packet.PiGlobalID
        resource = packet.AllocatedResource
        pi_email = packet.PiEmail
        board_type = packet.BoardType
        request_type = packet.RequestType
        pi_organization = packet.PiOrganization
        #   pi_nsf_status_code = packet.PiNsfStatusCode

        #        if packet.PiPersonID and packet.PiRemoteSiteLogin:
        #            pi_person_id = packet.PiPersonID         # site person_id for the PI (if known)
        #            pi_login = packet.PiRemoteSiteLogin  # login on resource for the PI (if known)
        #        else:
        person_id = [
            x["PersonID"] for x in packet.SitePersonId if x["Site"] == "X-PORTAL"
        ][0]
        pi_person_id = self.u.create_openstack_account(person_id, pi_email)
        pi_login = person_id
        if (
            allocation_type == "new"
            or allocation_type == "renewal"
            or allocation_type == "supplement"
            or allocation_type == "transfer"
        ):
            existing_project_list = self.u.find_project(
                charge_code, start_date, end_date, resource
            )
            if (
                allocation_type == "supplement" or allocation_type == "transfer"
            ) and len(existing_project_list) > 0:
                self.u.supplement_project(
                    project_id, resource, amount, start_date, end_date
                )
            else:
                self.u.create_project(
                    charge_code,
                    resource,
                    start_date,
                    end_date,
                    abstract,
                    project_title,
                    pi_person_id,
                    amount,
                    allocation_type,
                    board_type,
                    request_type,
                    pi_organization,
                )
                self.u.user_group_add(charge_code, pi_person_id)
        #        elif allocation_type == 'renewal':
        #                self.u.renew_project(project_id, resource, start_date, end_date, abstract, project_title)
        #        elif allocation_type == 'transfer':
        #                #if DNE must be positive and treated as new
        #                pass
        elif allocation_type == "adjustment":
            self.u.adjust_project(project_id, resource, amount, start_date)
        elif allocation_type == "extension":
            self.u.extend_project(project_id, resource, start_date, end_date)
        else:
            raise
        slack_data = {
            "text": "Processed %s for %s on %s"
            % (allocation_type, charge_code, resource)
        }
        response = requests.post(
            self.slack_webhook_url,
            data=json.dumps(slack_data),
            headers={"Content-Type": "application/json"},
        )
        npc = packet.reply_packet()
        npc.ProjectID = project_id  # local project ID
        npc.PiRemoteSiteLogin = pi_login  # local login for the PI on the resource
        npc.PiPersonID = pi_person_id  # local person ID for the pi

        # send the NPC
        self.amie_client.send_packet(npc)


if __name__ == "__main__":
    pp = PacketProcessor()
    if sys.argv[1] == "accounts":
        pp.process_packets(
            pp.packet_filter(
                pp.amie_client.list_packets().packets,
                {"type": "request_account_create"},
            )
        )
        pp.process_packets(
            pp.packet_filter(
                pp.amie_client.list_packets().packets, {"type": "data_account_create"}
            )
        )
    elif sys.argv[1] == "all":
        pp.process_packets(pp.amie_client.list_packets().packets)
        pp.process_packets(pp.amie_client.list_packets().packets)
