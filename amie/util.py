from keystoneauth1.identity import v3
from keystoneauth1 import session
import keystoneauth1.exceptions.http
from keystoneclient.v3 import client
from novaclient import client as novaclient
from neutronclient.v2_0 import client as neutronclient
from cinderclient import client as cinderclient
import designateclient.client
from sqlalchemy.orm import sessionmaker
import subprocess

# from amie import model
import model


class Util:
    def __init__(self, testing=False, db_connection="", conf=None):
        if not conf:
            conf = {
                "os_auth_url": "http://localhost:5000/",
                "os_user_id": "admin",
                "os_password": "password",
                "os_project_id": "admin",
            }
        auth = v3.Password(
            auth_url=conf["os_auth_url"],
            username=conf["os_username"],
            password=conf["os_password"],
            project_name=conf["os_project_name"],
            user_domain_name=conf["os_user_domain"],
            project_domain_name=conf["os_project_domain"],
        )
        sess = session.Session(auth=auth)
        self.keystone = client.Client(session=sess, region_name=conf["os_region_name"])
        self.nova = novaclient.Client(
            "2.88", session=sess, region_name=conf["os_region_name"]
        )
        self.neutron = neutronclient.Client(
            session=sess, region_name=conf["os_region_name"]
        )
        self.cinder = cinderclient.Client(
            session=sess, version=3, region_name=conf["os_region_name"]
        )
        self.designate = designateclient.client.Client(
            "2", session=sess, region_name=conf["os_region_name"]
        )
        if testing:
            self.engine = model.create_engine("sqlite:///:memory:")
            model.Project.metadata.create_all(self.engine)
        else:
            self.engine = model.create_engine(db_connection)
        Session = sessionmaker(bind=self.engine)
        self.session = Session()
        self.quota_defaults = {
            "startup": {
                "jetstream2.indiana.xsede.org": {
                    "cores": 150,
                    "ram": 599040,
                    "floating_ip": 10,
                    "instances": 0,
                },
                "jetstream2-gpu.indiana.xsede.org": {
                    "cores": 64,
                    "ram": 256000,
                    "floating_ip": 4,
                    "instances": 0,
                },
                "jetstream2-lm.indiana.xsede.org": {
                    "cores": 128,
                    "ram": 1024000,
                    "floating_ip": 2,
                    "instances": 0,
                },
                "jetstream2-storage.indiana.xsede.org": {
                    "cores": 0,
                    "ram": 0,
                    "floating_ip": 0,
                    "instances": 0,
                },
            },
            "educational": {
                "jetstream2.indiana.xsede.org": {
                    "cores": 200,
                    "ram": 798720,
                    "floating_ip": 25,
                    "instances": 50,
                },
                "jetstream2-gpu.indiana.xsede.org": {
                    "cores": 320,
                    "ram": 1280000,
                    "floating_ip": 25,
                    "instances": 0,
                },
                "jetstream2-lm.indiana.xsede.org": {
                    "cores": 128,
                    "ram": 1024000,
                    "floating_ip": 2,
                    "instances": 0,
                },
                "jetstream2-storage.indiana.xsede.org": {
                    "cores": 0,
                    "ram": 0,
                    "floating_ip": 0,
                    "instances": 0,
                },
            },
            "jta": {
                "jetstream2.indiana.xsede.org": {
                    "cores": 2,
                    "ram": 6144,
                    "floating_ip": 2,
                    "instances": 2,
                },
                "jetstream2-gpu.indiana.xsede.org": {
                    "cores": 0,
                    "ram": 1280000,
                    "floating_ip": 0,
                    "instances": 0,
                },
                "jetstream2-lm.indiana.xsede.org": {
                    "cores": 0,
                    "ram": 0,
                    "floating_ip": 0,
                    "instances": 0,
                },
                "jetstream2-storage.indiana.xsede.org": {
                    "cores": 0,
                    "ram": 0,
                    "floating_ip": 0,
                    "instances": 0,
                },
            },
        }
        self.startup_thresholds = {
            "jetstream2.indiana.xsede.org": 200000,
            "jetstream2-lm.indiana.xsede.org": 400000,
            "jetstream2-gpu.indiana.xsede.org": 600000,
            "jetstream2-storage.indiana.xsede.org": 1,
        }

    def create_openstack_account(self, requested_login, email, login=None):
        """Given a requested login and email create an openstack user and return the uuid"""

        xsede_domain = self.keystone.domains.find(name="ACCESS").id
        if login:
            user = self.keystone.users.find(
                name=login + "@access-ci.org", domain=xsede_domain
            )
        else:
            login_list = self.keystone.users.list(
                name=requested_login + "@access-ci.org", domain=xsede_domain
            )
            if len(login_list) == 0:
                user = self.keystone.users.create(
                    requested_login + "@access-ci.org", domain=xsede_domain, email=email
                )
                subprocess.getoutput(
                    """echo "ADD js2-users-l %s jetstream2user" | mail -r "jeremy@iu.edu" list@iu.edu"""
                    % (email,)
                )

            else:
                user = login_list[0]
        return user.id

    def create_project(
        self,
        charge_code,
        resource,
        start_date,
        end_date,
        abstract,
        title,
        pi_uuid,
        award,
        allocation_type,
        board_type,
        request_type,
        pi_organization,
    ):
        """Given project metadata persist this data in the database and create an openstack
        project, group, and role on the project for the group"""
        balance = award  # assuming we are starting with the full award amount
        allocation_list = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_code,
                model.Project.resource == resource,
            )
            .all()
        )
        project_record = model.Project(
            charge_number=charge_code,
            resource=resource,
            start_date=start_date,
            end_date=end_date,
            abstract=abstract,
            project_title=title,
            pi_uuid=pi_uuid,
            service_units_allocated=award,
            allocation_type=model.ALLOCATION_TYPES(allocation_type.lower()),
            board_type=model.BOARD_TYPES(board_type.lower().replace(" ", "")),
            request_type=model.REQUEST_TYPES(request_type.lower()),
            pi_organization=pi_organization,
            active=1,
        )
        self.session.add(project_record)
        self.create_openstack_project(
            charge_code,
            title,
            allocation_list,
            resource,
            board_type,
            award,
            allocation_type,
            jta=False,
        )
        self.enable_project(charge_code)
        self.session.commit()

    def create_openstack_project(
        self,
        charge_code,
        title,
        allocation_list,
        resource,
        board_type,
        award,
        allocation_type,
        jta,
    ):
        user_role_id = self.keystone.roles.find(name="member").id
        if not jta:
            project_list = self.keystone.projects.list(
                name=charge_code, domain_name="ACCESS"
            )
            domain = self.keystone.domains.find(name="ACCESS")
        else:
            project_list = self.keystone.projects.list(
                name=charge_code, domain_name="jta"
            )
            domain = self.keystone.domains.find(name="jta")
        if len(project_list) == 0:
            project = self.keystone.projects.create(
                name=charge_code, domain=domain, description=title
            )
            endpoint_group = self.keystone.endpoint_groups.find(name="IU")
            self.keystone.endpoint_filter.add_endpoint_group_to_project(
                endpoint_group.id, project.id
            )
        else:
            project = project_list[0]
        group_list = self.keystone.groups.list(
            name=charge_code, domain_name=domain.name
        )
        if len(group_list) == 0:
            group = self.keystone.groups.create(name=charge_code, domain=domain)
        else:
            group = group_list[0]
        admin = self.keystone.users.find(name="admin")
        self.keystone.roles.grant(group=group.id, project=project.id, role=user_role_id)
        self.keystone.roles.grant(user=admin.id, project=project.id, role=user_role_id)
        flavorlist = {}
        if resource == "jetstream2-gpu.indiana.xsede.org" and not jta:
            flavorlist = {
                "g3.xl": "13",
                "g3.large": "12",
                "g3.medium": "11",
                "g3.small": "10",
            }
        elif resource == "jetstream2-lm.indiana.xsede.org" and not jta:
            flavorlist = {"r3.large": "14", "r3.xl": "15"}
        elif resource == "jetstream2.indiana.xsede.org":
            flavorlist = {
                "m3.tiny": "1",
                "m3.small": "2",
                "m3.quad": "3",
                "m3.medium": "4",
                "m3.large": "5",
                "m3.xl": "7",
                "m3.2xl": "8",
            }
        for flavor in flavorlist:
            if not project.id in [
                x.tenant_id
                for x in self.nova.flavor_access.list(flavor=flavorlist[flavor])
            ]:
                self.nova.flavor_access.add_tenant_access(
                    flavorlist[flavor], project.id
                )
        if len(allocation_list) == 0:
            self.set_quotas(
                project.id, board_type.lower(), resource, award, allocation_type
            )
        if not jta:
            self.create_zone(project.id, charge_code, resource)
            self.create_auto_allocated_topology(project.id, charge_code, resource)

    def create_zone(self, project_id, charge_code, resource):
        if resource == "jetstream2-storage.indiana.xsede.org":
            return
        dns_domain = charge_code.lower() + ".projects.jetstream-cloud.org."
        self.designate.session.sudo_project_id = project_id
        if self.designate.zones.list(criterion={"name": dns_domain}):
            self.designate.session.sudo_project_id = None
            return
        self.designate.session.sudo_project_id = None
        zone = self.designate.zones.create(
            dns_domain, email="jsadmin@jetstream-cloud.org"
        )
        xfer = self.designate.zone_transfers.create_request(zone["id"], project_id)
        self.designate.session.sudo_project_id = project_id
        acceptance = self.designate.zone_transfers.accept_request(
            xfer["id"], xfer["key"]
        )
        self.designate.session.sudo_project_id = None

    def create_auto_allocated_topology(self, project_id, charge_code, resource):
        if resource == "jetstream2-storage.indiana.xsede.org":
            return
        auto_allocated_topology = self.neutron.get_auto_allocated_topology(project_id)
        dns_domain = charge_code.lower() + ".projects.jetstream-cloud.org."
        network = self.neutron.show_network(
            auto_allocated_topology["auto_allocated_topology"]["id"]
        )
        network = self.neutron.update_network(
            network["network"]["id"], {"network": {"dns_domain": dns_domain}}
        )

    def user_group_add(self, group, user):
        """Add an openstack user to a group"""
        gid = self.keystone.groups.list(name=group)[0].id
        self.keystone.users.add_to_group(user, gid)

    def user_group_remove(self, user, group):
        """Remove an openstack user from a group"""
        gid = self.keystone.groups.list(name=group)[0].id
        try:
            self.keystone.users.remove_from_group(user, gid)
        except keystoneauth1.exceptions.http.NotFound:
            pass

    def renew_project(
        self, grant_id, resource, start_date, end_date, abstract, project_title
    ):
        # new allocation, replace amount,start date, and end date
        old_p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == grant_id,
                model.Project.resource == resource,
            )
            .order_by(model.Project.start_date)
            .one()
        )
        new_p = model.Project(
            charge_number=grant_id,
            resource=resource,
            start_date=start_date,
            end_date=end_date,
            abstract=abstract,
            project_title=project_title,
            active=1,
        )

        self.session.commit()

    def find_project(self, charge_number, start_date, end_date, resource):
        p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number,
                model.Project.resource == resource,
                model.Project.start_date == start_date,
                model.Project.end_date == end_date,
            )
            .order_by(model.Project.start_date)
            .all()
        )
        return p

    def supplement_project(self, charge_number, resource, amount, start_date, end_date):
        """Supplement a project by adding the designated amount ot the awward"""
        p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number,
                model.Project.resource == resource,
                model.Project.start_date == start_date,
                model.Project.end_date == end_date,
            )
            .order_by(model.Project.start_date)
            .one()
        )
        p.service_units_allocated += float(amount)
        p.active = 1
        self.session.commit()
        keystone_project = self.keystone.projects.find(name=charge_number)
        self.keystone.projects.update(keystone_project.id, enabled=True)

    def adjust_project(self, charge_number, resource, amount, start_date):
        """Adjust the amount awarded to a project by given amount"""
        p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number,
                model.Project.resource == resource,
            )
            .order_by(model.Project.start_date)
            .one()
        )
        p.service_units_allocated += float(amount)
        self.session.commit()

    def extend_project(self, charge_number, resource, start_date, end_date):
        """Extend a project by changing the end date"""
        p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number,
                model.Project.resource == resource,
                model.Project.start_date == start_date,
            )
            .order_by(model.Project.start_date)
            .one()
        )
        p.end_date = end_date
        p.active = 1
        self.enable_project(charge_number)
        self.session.commit()

    def update_email(self, person_id, email):
        """Set the openstack users email address"""
        try:
            self.keystone.users.update(person_id, email=email)
        except keystoneauth1.exceptions.http.NotFound:
            pass

    def inactive_project_list(self):
        inactive = (
            self.session.query(model.Project).filter(model.Project.active == 0).all()
        )
        inactive = list(set([x.charge_number for x in inactive]))
        active = (
            self.session.query(model.Project).filter(model.Project.active == 1).all()
        )
        active = list(set([x.charge_number for x in active]))
        return [x for x in inactive if not x in active]

    def disable_project(self, charge_number, resource, start_date, end_date):
        """Mark the openstack project as disabled"""
        project = self.keystone.projects.find(name=charge_number)
        p = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number,
                model.Project.resource == resource,
                model.Project.end_date == end_date,
                model.Project.start_date == start_date,
            )
            .order_by(model.Project.start_date)
            .one()
        )
        p.active = 0
        self.session.commit()
        l = (
            self.session.query(model.Project)
            .filter(
                model.Project.charge_number == charge_number, model.Project.active == 1
            )
            .all()
        )
        if len(l) == 0:
            self.keystone.projects.update(project.id, enabled=False)

    def enable_project(self, project):
        """Mark the openstack project as enabled"""
        p = self.keystone.projects.find(name=project)
        self.keystone.projects.update(p, enabled=True)

    def set_quotas(
        self, project_id, boardtype, resource, amount, allocation_type="new"
    ):
        amount = abs(float(amount))
        if not boardtype.lower() in self.quota_defaults:
            scaling_factor = amount / self.startup_thresholds[resource]
            boardtype = "startup"
        else:
            scaling_factor = 1
        if boardtype.lower() == "jta":
            op = lambda x, y: x
        else:
            op = lambda x, y: x + y
        nova_quotas = self.nova.quotas.get(project_id)
        nova_quotas.cores = op(
            int(
                scaling_factor
                * self.quota_defaults[boardtype.lower()][resource]["cores"]
            ),
            nova_quotas.cores,
        )
        nova_quotas.ram = op(
            int(
                scaling_factor * self.quota_defaults[boardtype.lower()][resource]["ram"]
            ),
            nova_quotas.ram,
        )
        nova_quotas.instances = op(
            int(
                scaling_factor
                * self.quota_defaults[boardtype.lower()][resource]["instances"]
            ),
            nova_quotas.instances,
        )
        self.nova.quotas.update(
            project_id,
            cores=nova_quotas.cores,
            ram=nova_quotas.ram,
            instances=nova_quotas.instances,
        )
        neutron_quotas = self.neutron.show_quota(project_id)
        neutron_quotas["quota"]["floatingip"] = op(
            int(
                scaling_factor
                * self.quota_defaults[boardtype.lower()][resource]["floating_ip"]
            ),
            neutron_quotas["quota"]["floatingip"],
        )
        self.neutron.update_quota(project_id, neutron_quotas)
        if resource == "jetstream2-storage.indiana.xsede.org":
            if allocation_type == "supplement":
                amount += self.cinder.quotas.get(project_id).gigabytes
            self.cinder.quotas.update(project_id, gigabytes=int(amount))
