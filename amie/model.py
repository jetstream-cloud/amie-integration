import enum
from sqlalchemy import (
    create_engine,
    Table,
    Column,
    Integer,
    Date,
    String,
    MetaData,
    Text,
    Float,
    DateTime,
    JSON,
    Enum,
    ForeignKey,
)

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

# meta = MetaData()


class ALLOCATION_TYPES(enum.Enum):
    new = "new"
    renewal = "renewal"
    advance = "advance"
    supplement = "supplement"
    transfer = "transfer"
    extension = "extension"
    adjustment = "adjustment"


class BOARD_TYPES(enum.Enum):
    startup = "startup"
    research = "research"
    educational = "educational"
    campuschampions = "campuschampions"
    discretionary = "discretionary"
    staff = "staff"
    x2staff = "xsede2 staff allocations"
    xsede2staffallocations = "xsede2staffallocations"
    explore = "explore"
    discover = "discover"
    accelerate = "accelerate"
    staffallocations = "staffallocations"
    maximize = "maximize"


class REQUEST_TYPES(enum.Enum):
    new = "new"
    renewal = "renewal"


ALLOCATED_RESOURCES = (
    "jetstream2.indiana.xsede.org",
    "jetstream2-lm.indiana.xsede.org",
    "jetstream2-gpu.indiana.xsede.org",
    "jetstream2-storage.indiana.xsede.org",
)


class Project(Base):
    __tablename__ = "projects"
    id = Column(Integer, primary_key=True)
    charge_number = Column(String(16))
    resource = Column(String(64))
    start_date = Column(Date)
    end_date = Column(Date)
    abstract = Column(Text)
    project_title = Column(Text)
    pi_uuid = Column(String(36))
    service_units_allocated = Column(Float)
    allocation_type = Column(Enum(ALLOCATION_TYPES))
    board_type = Column(Enum(BOARD_TYPES))
    request_type = Column(Enum(REQUEST_TYPES))
    pi_organization = Column(String(64))
    active = Column(Integer)


class Balance(Base):
    __tablename__ = "balances"
    project_record_id = Column(Integer, ForeignKey("projects.id"), primary_key=True)
    used = Column(Float)
    last_updated = Column(DateTime)


class PacketLog(Base):
    __tablename__ = "packet_log"
    transaction_id = Column(Integer, primary_key=True)
    packet_id = Column(Integer, primary_key=True)
    packet_timestamp = Column(DateTime, primary_key=True)
    data = Column(JSON, nullable=True)


class Usage(Base):
    __tablename__ = "usage"
    charge = Column(Float)
    end_time = Column(DateTime)
    local_project_id = Column(String(16))
    local_record_id = Column(Integer, primary_key=True)
    local_reference = Column(String(64))
    resource = Column(String(64))
    start_time = Column(DateTime)
    username = Column(Text)
    collection_time = Column(DateTime)
    submit_time = Column(DateTime)
