import argparse
from datetime import datetime, timedelta
import pprint
import pymysql
import usage
import os
from sqlalchemy.engine.url import make_url

# Run `python3 run_usage.py --help` for help on how to use this


def get_env_var(env_var_name):
    val = os.environ.get(env_var_name)
    if val is not None:
        return val
    else:
        raise KeyError("Environment variable {} is not defined".format(env_var_name))


def db_conn_dict_from_url(url):
    parsed_url = make_url(url)
    db_conn_dict = pymysql.connect(
        host=parsed_url.host,
        user=parsed_url.username,
        password=parsed_url.password,
        database=parsed_url.database,
        cursorclass=pymysql.cursors.DictCursor,
    )
    return db_conn_dict


def keystone_db_conn_dict():
    url = get_env_var("OS_KEYSTONE_DB_URL")
    return db_conn_dict_from_url(url)


def nova_db_conn_dict():
    url = get_env_var("OS_NOVA_DB_URL")
    return db_conn_dict_from_url(url)


def nova_api_db_conn_dict():
    url = get_env_var("OS_NOVA_API_DB_URL")
    return db_conn_dict_from_url(url)


def usage_db_conn_dict():
    url = get_env_var("USAGE_DB_URL")
    return db_conn_dict_from_url(url)


def get_domain_id():
    return get_env_var("KEYSTONE_DOMAIN_ID")


def usage_for_instance(args):
    pprint.pprint(
        usage.get_total_charges_for_instance(
            nova_db_conn_dict(),
            nova_api_db_conn_dict(),
            args.instance_uuid,
            args.start,
            args.end,
        )
    )


def generate_records(args):
    if args.write_to_db:
        usage.generate_and_write_records_to_db(
            nova_db_conn_dict(),
            nova_api_db_conn_dict(),
            keystone_db_conn_dict(),
            usage_db_conn_dict(),
            get_domain_id(),
            args.start,
            args.end,
        )
    else:
        pprint.pprint(
            usage.generate_records(
                nova_db_conn_dict(),
                nova_api_db_conn_dict(),
                keystone_db_conn_dict(),
                get_domain_id(),
                args.start,
                args.end,
            )
        )


def upload_records(args):
    usage.upload_records(
        keystone_db_conn_dict(),
        usage_db_conn_dict(),
        get_env_var("AMIE_API_URL"),
        get_env_var("AMIE_API_SITE"),
        get_env_var("AMIE_API_KEY"),
        args.dry_run,
    )


def reupload_failed_records(args):
    usage.reupload_failed_records(
        keystone_db_conn_dict(),
        usage_db_conn_dict(),
        get_env_var("AMIE_API_URL"),
        get_env_var("AMIE_API_SITE"),
        get_env_var("AMIE_API_KEY"),
        args.dry_run,
    )


def update_balances(args):
    usage.update_balances(keystone_db_conn_dict(), usage_db_conn_dict(), args.dry_run)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(help="sub-command")

    # parent_parser lets us apply a common set of arguments (here start and end datetimes) to multiple subcommand parsers
    # per https://stackoverflow.com/questions/7498595/python-argparse-add-argument-to-multiple-subparsers
    parent_parser = argparse.ArgumentParser(add_help=False)
    parent_parser.add_argument(
        "--start",
        type=datetime.fromisoformat,
        metavar="<YYYY-MM-DD>",
        help="start of interval as 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM:SS', time in UTC",
    )
    parent_parser.add_argument(
        "--end",
        type=datetime.fromisoformat,
        metavar="<YYYY-MM-DD>",
        help="end of interval as 'YYYY-MM-DD' or 'YYYY-MM-DD HH:MM:SS', time in UTC",
    )

    parser_usage_for_instance = subparsers.add_parser(
        "usage-for-instance", parents=[parent_parser]
    )
    parser_usage_for_instance.add_argument(
        "instance_uuid", help="UUID of instance for which to return usage information"
    )

    parser_usage_for_instance.set_defaults(func=usage_for_instance)

    parser_generate_records = subparsers.add_parser(
        "generate-records", parents=[parent_parser]
    )

    parser_generate_records.add_argument(
        "--write-to-db",
        action="store_true",
        help="Write usage records to database instead of printing to stdout",
    )

    parser_generate_records.set_defaults(func=generate_records)

    parser_upload_records = subparsers.add_parser(
        "upload-records", parents=[parent_parser]
    )

    parser_upload_records.add_argument(
        "--dry-run",
        action="store_true",
        help="Print compute usage records to stdout instead of uploading to AMIE usage API",
    )

    parser_upload_records.set_defaults(func=upload_records)

    parser_reupload_failed_records = subparsers.add_parser(
        "reupload-failed-records", parents=[parent_parser]
    )

    parser_reupload_failed_records.add_argument(
        "--dry-run",
        action="store_true",
        help="Print instance overrides and new usage records to stdout, instead of writing to DB and uploading to AMIE usage API",
    )

    parser_reupload_failed_records.set_defaults(func=reupload_failed_records)

    parser_update_balances = subparsers.add_parser(
        "update-balances", parents=[parent_parser]
    )

    parser_update_balances.add_argument(
        "--dry-run",
        action="store_true",
        help="Print new project balances instead of writing to DB",
    )

    parser_update_balances.set_defaults(func=update_balances)

    args = parser.parse_args()
    try:
        args.func(args)
    except AttributeError as e:
        print("Error: '{}'. Did you forget to specify a subcommand?".format(e))
