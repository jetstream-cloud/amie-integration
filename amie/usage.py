import functools
from datetime import datetime
from itertools import chain
import amieclient
import pprint

"""
Data types

An event is a dict that looks like this

{'timestamp': datetime.datetime(2022, 2, 14, 16, 41, 2),
 'event': 'create'}

It can also look like this

{'timestamp': datetime.datetime(2022, 2, 14, 16, 41, 2),
 'event': 'resize',
 'old_flavor_id': '11'
 'new_flavor_id': '42'
 }


A state interval is a dict that looks like this

{'start_timestamp': datetime.datetime(2022, 2, 14, 16, 41, 2),
 'end_timestamp': datetime.datetime(2022, 2, 16, 18, 14, 36),
 'state': 'active',
 'flavor_id': '42',
 'billing_active': true,
 }

The end_timestamp can be None, in which case the end of the state interval is undefined (i.e. it likely continues to present).

The flavor_id can also be None, e.g. when billing interval is active but the instance is not yet created.

A list of state intervals comprises the history of an instance.

A charge is a dict that looks like this:

{'resource_type': 'jetstream2.indiana.xsede.org',
 'charge': 48239
 }

A record (i.e. usage record) is a dict that looks like this:

{'end_timestamp': datetime.datetime(2022, 6, 2, 0, 0, 0),
 'instance_uuid': '5ca1ab1e-f00d-caf0-beef-50de1ec7ab1e',
 'project_uuid': '15907f7e-8d1d-4d9d-8f21-d3e56d71e806',
 'charge': 48.25,
 'resource_type': 'jetstream2.indiana.xsede.org',
 'start_timestamp': datetime.datetime(2022, 6, 1, 0, 0, 0),
 'user_uuid': '1d63f8e4-b26a-4c39-a423-a779667bda2b'
 }


"""

events_relevant_to_usage = [
    "create",
    "delete",
    "pause",
    "resume",
    "shelve",
    "start",
    "stop",
    "suspend",
    "unpause",
    "unshelve",
]

event_to_state_lookup = {
    "create": "active",
    "delete": "deleted",
    "resume": "active",
    "shelve": "shelved_offloaded",
    "start": "active",
    "stop": "stopped",
    "suspend": "suspended",
    "unshelve": "active",
}

state_charge_multipliers = {
    "active": 1,
    "deleted": 0,
    "error": 0,
    "not_yet_created": 0,
    "paused": 0.75,
    "resized": 1,
    "shelved_offloaded": 0,
    "stopped": 0.5,
    "suspended": 0.75,
}

flavor_type_charge_multipliers = {
    "m3": 1,
    "g3": 2,
    "g3p": 0,
    "r3": 2,
    "p3": 1,
}

flavor_type_to_resource_type = {
    "m3": "jetstream2.indiana.xsede.org",
    "g3": "jetstream2-gpu.indiana.xsede.org",
    "g3p": "jetstream2-gpu.indiana.xsede.org",
    "r3": "jetstream2-lm.indiana.xsede.org",
    "p3": "jetstream2-lm.indiana.xsede.org",
}

# Some flavors have been deleted and new flavors replace them.
# This dict maps old (deleted) flavor IDs to their new equivalent,
# and the new flavor is used to calculate the charge.

flavor_id_map = {
    78: 125,  # m3.tiny
    18: 122,  # m3.small
    21: 119,  # m3.quad
    15: 116,  # m3.medium
    24: 113,  # m3.large
    27: 110,  # m3.xl
    30: 107,  # m3.2xl
}


def get_non_resize_events_for_instance(db_conn_dict, instance_uuid):
    query_results = get_non_resize_events_for_instance_sql_query_(
        db_conn_dict, instance_uuid
    )
    events = get_non_resize_events_for_instance_query_results_to_events_(query_results)
    return events


def get_non_resize_events_for_instance_sql_query_(db_conn_dict, instance_uuid):
    sql = "SELECT created_at, action FROM instance_actions WHERE action IN %s AND instance_uuid=%s"
    with db_conn_dict.cursor() as cursor:
        cursor.execute(sql, (events_relevant_to_usage, instance_uuid))
        return cursor.fetchall()


def get_non_resize_events_for_instance_query_results_to_events_(results):
    def result_to_event_dict(result):
        # Returns an event in a dict format like:
        # {'timestamp': datetime.datetime(2022, 2, 14, 16, 41, 2), 'event': 'create'}
        return {
            "timestamp": result.get("created_at"),
            "event": result.get("action"),
        }

    return list(map(result_to_event_dict, results))


def get_flavor_history_for_instance(db_conn_dict, instance_uuid):
    # Returns a dict with keys:
    # - initial_flavor_id with value containing initial flavor ID
    # - resize_events with value containing a list of resize events

    def migration_result_to_dict(result):
        return {
            "created_timestamp": result.get("created_at"),
            "updated_timestamp": result.get("updated_at"),
            "status": result.get("status"),
            "old_flavor_id": result.get("old_instance_type_id"),
            "new_flavor_id": result.get("new_instance_type_id"),
        }

    sql = "SELECT created_at, updated_at, status, old_instance_type_id, new_instance_type_id FROM migrations WHERE migration_type = 'resize' AND instance_uuid=%s"
    with db_conn_dict.cursor() as cursor:
        cursor.execute(sql, instance_uuid)
        results = cursor.fetchall()
    migrations = map(migration_result_to_dict, results)

    def migration_to_resize_events(migration):
        if migration.get("status") in ["finished", "confirmed"]:
            return [
                {
                    "timestamp": migration.get("created_timestamp"),
                    "event": "resize",
                    "old_flavor_id": migration.get("old_flavor_id"),
                    "new_flavor_id": migration.get("new_flavor_id"),
                }
            ]
        elif migration.get("status") == "reverted":
            # Treat a reverted migration as two resize events
            return [
                # The first happens when the migration is initiated
                {
                    "timestamp": migration.get("created_timestamp"),
                    "event": "resize",
                    "old_flavor_id": migration.get("old_flavor_id"),
                    "new_flavor_id": migration.get("new_flavor_id"),
                },
                # The second happens when the migration is reverted
                {
                    "timestamp": migration.get("updated_timestamp"),
                    "event": "resize",
                    "old_flavor_id": migration.get("new_flavor_id"),
                    "new_flavor_id": migration.get("old_flavor_id"),
                },
            ]
        elif migration.get("status") in ["pre-migrating", "error"]:
            # These are not considered a resize for billing purposes
            return []
        else:
            raise Exception(
                "This code doesn't know how to handle migration status '"
                + migration.get("status")
                + "'"
            )

    resize_events_from_migrations = map(migration_to_resize_events, migrations)
    resize_events_flat_list = list(chain.from_iterable(resize_events_from_migrations))

    def initial_flavor_id():
        try:
            first_resize_event = resize_events_flat_list[0]
            return first_resize_event.get("old_flavor_id")
        except IndexError:
            # No resize events, so the current flavor _is_ the initial flavor
            sql = "SELECT instance_type_id FROM instances WHERE uuid=%s"
            with db_conn_dict.cursor() as cursor:
                cursor.execute(sql, instance_uuid)
                try:
                    flavor_id = cursor.fetchone().get("instance_type_id")
                except AttributeError as e:
                    raise Exception(
                        "Could not find flavor ID for instance. Instance UUID may be invalid."
                    )
                return flavor_id

    return {
        "initial_flavor_id": initial_flavor_id(),
        "resize_events": resize_events_flat_list,
    }


def instance_create_time(nova_db_conn_dict, instance_uuid):
    """Returns the time that an instance was created."""
    sql = "SELECT created_at from nova.instances WHERE uuid = %s"
    with nova_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, instance_uuid)
        result = cursor.fetchone()
    return result.get("created_at")


def get_synthetic_create_event(nova_db_conn_dict, instance_uuid):
    """Returns a create event for an instance, even if a create event does not exist in the nova.instance_actions table."""
    create_time = instance_create_time(nova_db_conn_dict, instance_uuid)
    if create_time:
        event = {"timestamp": create_time, "event": "create"}
        return event
    else:
        raise Exception("This instance has no creation time in the Nova database")


def instance_delete_time(nova_db_conn_dict, instance_uuid):
    """Returns the time that an instance was deleted, if it is deleted. If the instance is not deleted, returns None."""
    sql = "SELECT deleted_at from nova.instances WHERE uuid = %s"
    with nova_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, instance_uuid)
        result = cursor.fetchone()
    return result.get("deleted_at")


def get_synthetic_delete_event(nova_db_conn_dict, instance_uuid):
    """Returns a delete event if a given instance is deleted, even if a delete event does not exist in the nova.instance_actions table. Otherwise, returns None."""
    delete_time = instance_delete_time(nova_db_conn_dict, instance_uuid)
    if delete_time:
        event = {"timestamp": delete_time, "event": "delete"}
        return event
    else:
        return None


def combined_events_for_instance(db_conn_dict, instance_uuid, start, end):
    events = get_non_resize_events_for_instance(db_conn_dict, instance_uuid)
    flavor_history_dict = get_flavor_history_for_instance(db_conn_dict, instance_uuid)
    try:
        create_event = events[0]
    except IndexError:
        # Instance has no explicit create event, so make a synthetic one from create time in DB
        create_event = get_synthetic_create_event(db_conn_dict, instance_uuid)
    create_event_with_flavor = {
        **create_event,
        **{"new_flavor_id": flavor_history_dict.get("initial_flavor_id")},
    }
    try:
        events_after_create = events[1:] + flavor_history_dict.get("resize_events")
    except IndexError:
        # Handling case of no further events
        events_after_create = flavor_history_dict.get("resize_events")

    # Look for a delete event in the list of events. If there isn't one, then make a synthetic delete event if the instance is deleted.
    has_delete_event = False
    for event in events_after_create:
        if event.get("event") == "delete":
            has_delete_event = True
    if not has_delete_event:
        synthetic_delete_event = get_synthetic_delete_event(db_conn_dict, instance_uuid)
    else:
        synthetic_delete_event = None

    all_sorted_events = sorted(
        [create_event_with_flavor]
        + events_after_create
        + [get_billing_start_event(start)]
        + [get_billing_end_event(end)]
        + ([synthetic_delete_event] if synthetic_delete_event else []),
        key=lambda e: e.get("timestamp"),
    )
    return all_sorted_events


def get_billing_start_event(timestamp):
    # Default to "the beginning of time" if no start time is specified
    return dict(
        {
            "timestamp": (
                timestamp if timestamp is not None else datetime.fromtimestamp(0)
            ),
            "event": "billing_start",
        }
    )


def get_billing_end_event(timestamp):
    return dict({"timestamp": timestamp or datetime.now(), "event": "billing_end"})


def get_state_intervals_from_events(events):
    def process_next_state_interval(existing_intervals, next_event):
        # Given a list of state intervals (existing_intervals) and a next_event, return the new list of state intervals

        if len(existing_intervals) == 0:
            # First event will be either create or start of billing
            if next_event.get("event") == "create":
                return [
                    {
                        "start_timestamp": next_event.get("timestamp"),
                        "end_timestamp": None,
                        "state": "active",
                        "flavor_id": next_event.get("new_flavor_id"),
                        "billing_active": False,
                    }
                ]
            elif next_event.get("event") == "billing_start":
                return [
                    {
                        "start_timestamp": next_event.get("timestamp"),
                        "end_timestamp": None,
                        "state": "not_yet_created",
                        "flavor_id": None,
                        "billing_active": True,
                    }
                ]
            else:
                raise Exception(
                    "List of events must begin with either create or start of billing"
                )
        else:
            prev_interval = existing_intervals[-1]

            new_prev_interval = {
                "start_timestamp": prev_interval.get("start_timestamp"),
                "end_timestamp": next_event.get("timestamp"),
                "state": prev_interval.get("state"),
                "flavor_id": prev_interval.get("flavor_id"),
                "billing_active": prev_interval.get("billing_active"),
            }

            # TODO make logic read nicer until "End TODO"
            if len(existing_intervals) >= 2:
                new_existing_intervals = existing_intervals[:-1] + [new_prev_interval]
            else:
                new_existing_intervals = [new_prev_interval]
            # End TODO

            next_state = event_to_state_lookup.get(next_event.get("event"))

            # If next event doesn't define a new_flavor_id, keep using the old one
            next_interval_flavor_id = next_event.get(
                "new_flavor_id"
            ) or prev_interval.get("flavor_id")

            if next_event.get("event") == "billing_start":
                next_interval_billing_active = True
            elif next_event.get("event") == "billing_end":
                next_interval_billing_active = False
            else:
                next_interval_billing_active = prev_interval.get("billing_active")

            next_interval = {
                "start_timestamp": next_event.get("timestamp"),
                "end_timestamp": None,
                "state": (
                    next_state if next_state is not None else prev_interval.get("state")
                ),
                "flavor_id": next_interval_flavor_id,
                "billing_active": next_interval_billing_active,
            }

            new_intervals = new_existing_intervals + [next_interval]
            return new_intervals

    return list(functools.reduce(process_next_state_interval, events, []))


def get_flavor_charge_info(nova_api_db_conn_dict, flavor_id):
    # returns a dict with keys:
    # - multiplier, with integer value
    # - resource_type

    # Override with new flavor ID if needed
    override_flavor_id = flavor_id_map.get(flavor_id, flavor_id)

    def flavor_result_to_dict(result):
        return {
            "name": result.get("name"),
            "vcpus": result.get("vcpus"),
        }

    sql = "SELECT name, vcpus FROM flavors WHERE id = %s"
    with nova_api_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, override_flavor_id)
        flavor_result = cursor.fetchone()
    flavor = flavor_result_to_dict(flavor_result)

    flavor_type = flavor.get("name").split(".")[0]
    flavor_type_charge_multiplier = flavor_type_charge_multipliers.get(flavor_type)

    vcpus_charge_multiplier = flavor.get("vcpus")

    flavor_charge_multiplier = flavor_type_charge_multiplier * vcpus_charge_multiplier

    resource_type = flavor_type_to_resource_type.get(flavor_type)

    return {
        "multiplier": flavor_charge_multiplier,
        "resource_type": resource_type,
    }


def get_charge_for_state_interval(
    nova_db_conn_dict, nova_api_db_conn_dict, state_interval, end
):
    # returns either a dict with keys:
    # - resource_type, with value "regular", "GPU", or "LM"
    # - charge, with integer SU value
    # or None, if the state interval has no resource type to charge to

    if (
        state_interval.get("flavor_id") is None
        or state_interval.get("billing_active") is not True
    ):
        return None
    else:
        # If the end of the state interval is undefined, we assume it continues until the billing end time
        duration = (state_interval.get("end_timestamp") or end) - state_interval.get(
            "start_timestamp"
        )
        duration_in_hours = duration.total_seconds() / 3600
        state_charge_multiplier = state_charge_multipliers.get(
            state_interval.get("state")
        )
        flavor_charge_info = get_flavor_charge_info(
            nova_api_db_conn_dict, state_interval.get("flavor_id")
        )

        charge = (
            duration_in_hours
            * state_charge_multiplier
            * flavor_charge_info.get("multiplier")
        )

        rounded_charge = round(charge, 3)

        return {
            "resource_type": flavor_charge_info.get("resource_type"),
            "charge": rounded_charge,
        }


def get_total_charges_for_instance(
    nova_db_conn_dict, nova_api_db_conn_dict, instance_uuid, start, end
):
    # returns a list of dicts, each dict having keys:
    # - resource_type, with value "regular", "GPU", or "LM"
    # - charge, with integer SU value

    events = combined_events_for_instance(nova_db_conn_dict, instance_uuid, start, end)
    state_intervals = get_state_intervals_from_events(events)
    charges = map(
        lambda state_interval: get_charge_for_state_interval(
            nova_db_conn_dict, nova_api_db_conn_dict, state_interval, end
        ),
        state_intervals,
    )

    # TODO this could have nicer factoring
    regular = 0
    GPU = 0
    LM = 0
    for charge in charges:
        if charge is None:
            pass
        else:
            resource_type = charge.get("resource_type")
            charge = charge.get("charge")
            if resource_type == "jetstream2.indiana.xsede.org":
                regular += charge
            elif resource_type == "jetstream2-gpu.indiana.xsede.org":
                GPU += charge
            elif resource_type == "jetstream2-lm.indiana.xsede.org":
                LM += charge
            else:
                raise Exception("Unrecognized resource type of '" + resource_type + "'")

    charges_dicts = [
        {"resource_type": "jetstream2.indiana.xsede.org", "charge": regular},
        {"resource_type": "jetstream2-gpu.indiana.xsede.org", "charge": GPU},
        {"resource_type": "jetstream2-lm.indiana.xsede.org", "charge": LM},
    ]

    def is_charge_nonzero(charge_dict):
        return charge_dict.get("charge") != 0

    nonzero_charges_dicts = list(filter(is_charge_nonzero, charges_dicts))
    return nonzero_charges_dicts


def get_project_ids_in_domain(keystone_db_conn_dict, domain_id):
    sql = "SELECT id FROM project WHERE domain_id = %s"
    with keystone_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, domain_id)
        result_rows = cursor.fetchall()

    def result_row_to_project_id(row):
        return row.get("id")

    return list(map(result_row_to_project_id, result_rows))


def get_extant_instances(
    nova_db_conn_dict, keystone_db_conn_dict, domain_id, start, end
):
    sql = """SELECT uuid, created_at, deleted_at, vm_state, project_id, user_id FROM nova.instances WHERE created_at <= '%s' AND (deleted_at IS NULL OR deleted_at >= '%s')"""
    with nova_db_conn_dict.cursor() as cursor:
        cursor.execute(sql % (end, start))
        result_rows = cursor.fetchall()

    project_ids_in_domain = get_project_ids_in_domain(keystone_db_conn_dict, domain_id)

    def instance_project_is_in_domain(instance):
        return instance.get("project_id") in project_ids_in_domain

    return list(filter(instance_project_is_in_domain, result_rows))


def generate_records(
    nova_db_conn_dict,
    nova_api_db_conn_dict,
    keystone_db_conn_dict,
    domain_id,
    start,
    end,
):
    def record_for_instance(instance):
        charges = get_total_charges_for_instance(
            nova_db_conn_dict, nova_api_db_conn_dict, instance.get("uuid"), start, end
        )

        def charge_to_record(charge):
            return {
                "end_timestamp": end,
                "instance_uuid": instance.get("uuid"),
                "charge": charge.get("charge"),
                "project_uuid": instance.get("project_id"),
                "resource_type": charge.get("resource_type"),
                "start_timestamp": start,
                "user_uuid": instance.get("user_id"),
            }

        return list(map(charge_to_record, charges))

    instances = get_extant_instances(
        nova_db_conn_dict, keystone_db_conn_dict, domain_id, start, end
    )
    records_nested = map(record_for_instance, instances)
    records_flattened = list(chain.from_iterable(records_nested))

    return records_flattened


def generate_and_write_records_to_db(
    nova_db_conn_dict,
    nova_api_db_conn_dict,
    keystone_db_conn_dict,
    usage_db_conn_dict,
    domain_id,
    start,
    end,
):
    records = generate_records(
        nova_db_conn_dict,
        nova_api_db_conn_dict,
        keystone_db_conn_dict,
        domain_id,
        start,
        end,
    )

    def insert_record(record):
        sql = "INSERT INTO `usage` (start_time, end_time, project_uuid, user_uuid, resource_type, instance_uuid, charge) VALUES (%s, %s, %s, %s, %s, %s, %s)"
        values = (
            record.get("start_timestamp"),
            record.get("end_timestamp"),
            record.get("project_uuid"),
            record.get("user_uuid"),
            record.get("resource_type"),
            record.get("instance_uuid"),
            record.get("charge"),
        )
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql, values)
        usage_db_conn_dict.commit()

    list(map(insert_record, records))


def get_project_name_from_uuid(keystone_db_conn_dict, project_uuid):
    sql = "SELECT name FROM project WHERE id = %s"
    with keystone_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, project_uuid)
        try:
            project_name = cursor.fetchone().get("name")
        except AttributeError:
            raise Exception("Could not find project name, project UUID may be invalid.")
    return project_name


def username_without_domain_part(username):
    return username.replace("@access-ci.org", "")


def username_with_domain_part(username):
    if username.endswith("@access-ci.org"):
        return username
    else:
        return username + "@access-ci.org"


def get_user_name_from_uuid(keystone_db_conn_dict, user_uuid):
    sql = "SELECT name FROM local_user WHERE user_id = %s"
    with keystone_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, user_uuid)
        try:
            user_name = cursor.fetchone().get("name")
        except AttributeError:
            raise Exception("Could not find user name, user UUID may be invalid.")

    return username_without_domain_part(user_name)


def get_user_uuid_from_name(keystone_db_conn_dict, user_name):
    fully_qualified_username = username_with_domain_part(user_name)

    sql = "SELECT user_id FROM local_user WHERE name = %s"

    with keystone_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, fully_qualified_username)
        try:
            user_uuid = cursor.fetchone().get("user_id")
        except AttributeError:
            raise Exception("Could not find UUID from username.")

        return user_uuid


def get_pi_user_uuid_from_project_name(usage_db_conn_dict, project_name):
    # PI may have changed in project's history, so get the latest value first
    sql = "SELECT pi_uuid FROM projects WHERE charge_number = %s ORDER BY id DESC"
    with usage_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, project_name)
        try:
            pi_user_uuid = cursor.fetchone().get("pi_uuid")
        except AttributeError:
            raise Exception("Could not find PI user UUID from project name")
        return pi_user_uuid


def get_instance_override_user_uuid(usage_db_conn_dict, instance_uuid):
    sql = "SELECT user_uuid FROM usage_instance_override WHERE instance_uuid = %s"

    with usage_db_conn_dict.cursor() as cursor:
        cursor.execute(sql, instance_uuid)
        user_uuid = cursor.fetchone().get("user_uuid")
        return user_uuid


def set_instance_override_user_uuid(usage_db_conn_dict, instance_uuid, user_uuid):
    try:
        # Don't attempt to set in DB if record already exists
        get_instance_override_user_uuid(usage_db_conn_dict, instance_uuid)
    except AttributeError:
        sql = "INSERT INTO usage_instance_override (instance_uuid, user_uuid) VALUES (%s, %s)"
        values = (instance_uuid, user_uuid)
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql, values)
        usage_db_conn_dict.commit()


def upload_records(
    keystone_db_conn_dict,
    usage_db_conn_dict,
    amie_api_url,
    amie_api_site,
    amie_api_key,
    dry_run=False,
):
    amie_usage_client = amieclient.client.UsageClient(
        amie_api_site, amie_api_key, usage_url=amie_api_url
    )

    def get_unuploaded_usage_record_db_rows():
        sql = "SELECT id, start_time, end_time, project_uuid, user_uuid, resource_type, instance_uuid, charge FROM `usage` WHERE post_result IS NULL"
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetchall()

    def get_compute_usage_record(db_row):
        local_project_id = get_project_name_from_uuid(
            keystone_db_conn_dict, db_row.get("project_uuid")
        )

        # If we have already overridden the user for this instance, use override user (i.e. the allocation PI)
        try:
            user_uuid = get_instance_override_user_uuid(
                usage_db_conn_dict, db_row.get("instance_uuid")
            )
        except AttributeError:
            user_uuid = db_row.get("user_uuid")

        username = get_user_name_from_uuid(keystone_db_conn_dict, user_uuid)

        def datetime_to_api_acceptable_format(timestamp):
            return timestamp.isoformat() + "Z"

        return amieclient.usage.record.ComputeUsageRecord(
            charge=db_row.get("charge"),
            end_time=datetime_to_api_acceptable_format(db_row.get("end_time")),
            local_project_id=local_project_id,
            local_record_id=db_row.get("instance_uuid"),
            resource=db_row.get("resource_type"),
            start_time=datetime_to_api_acceptable_format(db_row.get("start_time")),
            submit_time=datetime_to_api_acceptable_format(db_row.get("start_time")),
            username=username_without_domain_part(username),
            node_count=1,
            local_reference=db_row.get("id"),
        )

    def upload_usage_record(compute_usage_record):
        responses = amie_usage_client.send(compute_usage_record)

        def handle_response(response):
            print(response.message)
            if not response.failed_records:
                sql = (
                    "UPDATE `usage` SET post_time = %s, post_result = %s WHERE id = %s"
                )
                values = (
                    datetime.now(),
                    "success",
                    compute_usage_record.local_reference,
                )
                with usage_db_conn_dict.cursor() as cursor:
                    cursor.execute(sql, values)
                    usage_db_conn_dict.commit()
            else:
                overall_message = response.message
                record_specific_error_message = response.failed_records[0].error
                print(record_specific_error_message)
                error_message_for_db = (
                    overall_message + " / " + record_specific_error_message
                )
                sql = "UPDATE `usage` SET post_time = %s, post_result = %s, post_error_message = %s WHERE id = %s"
                values = (
                    datetime.now(),
                    "failed",
                    error_message_for_db,
                    compute_usage_record.local_reference,
                )
                with usage_db_conn_dict.cursor() as cursor:
                    cursor.execute(sql, values)
                    usage_db_conn_dict.commit()

        list(map(handle_response, responses))

    unuploaded_usage_record_db_rows = get_unuploaded_usage_record_db_rows()
    compute_usage_records = list(
        map(get_compute_usage_record, unuploaded_usage_record_db_rows)
    )

    if dry_run:
        pprint.pprint(list(map(lambda r: r.as_dict(), compute_usage_records)))
    else:
        list(map(upload_usage_record, compute_usage_records))


def reupload_failed_records(
    keystone_db_conn_dict,
    usage_db_conn_dict,
    amie_api_url,
    amie_api_site,
    amie_api_key,
    dry_run=False,
):
    amie_usage_client = amieclient.client.UsageClient(
        amie_api_site, amie_api_key, usage_url=amie_api_url
    )

    def process_failed_record(record):
        error_msg = record.error
        if not error_msg.startswith("Allocation could not be found for user"):
            pass
        else:
            record_dict = record.as_dict()
            # check if user is PI
            record_username = record_dict.get("Username")
            record_project_name = record_dict.get("LocalProjectID")
            pi_user_uuid = get_pi_user_uuid_from_project_name(
                usage_db_conn_dict, record_project_name
            )
            pi_user_name = get_user_name_from_uuid(keystone_db_conn_dict, pi_user_uuid)
            if record_username == pi_user_name:
                # User is PI, don't try to do anything
                pass
            else:
                # User is not PI, override this instance setting user to PI, and re-upload new record
                instance_uuid = record_dict.get("LocalRecordID")
                if dry_run:
                    print(
                        "Would override instance "
                        + instance_uuid
                        + " with user UUID "
                        + pi_user_uuid
                    )
                else:
                    set_instance_override_user_uuid(
                        usage_db_conn_dict, instance_uuid, pi_user_uuid
                    )
                new_record = amieclient.usage.record.ComputeUsageRecord(
                    charge=record_dict.get("Charge"),
                    end_time=record_dict.get("EndTime"),
                    local_project_id=record_dict.get("LocalProjectID"),
                    local_record_id=record_dict.get("LocalRecordID"),
                    resource=record_dict.get("Resource"),
                    start_time=record_dict.get("StartTime"),
                    submit_time=record_dict.get("SubmitTime"),
                    # The one field that we are changing
                    username=pi_user_name,
                    node_count=1,
                    local_reference=record_dict.get("LocalReference"),
                )
                if dry_run:
                    print("Would upload usage record:")
                    pprint.pprint(new_record.as_dict())
                else:
                    amie_usage_client.send(new_record)

    failed_usage_records = amie_usage_client.get_failed_records()
    return list(map(process_failed_record, failed_usage_records.failed_records))


def update_balances(keystone_db_conn_dict, usage_db_conn_dict, dry_run=False):
    def get_projects():
        sql = "SELECT id, charge_number AS name, resource, start_date, end_date FROM projects"
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql)
            return cursor.fetchall()

    def get_project_uuid(project_name):
        sql = "SELECT id FROM keystone.project WHERE name = %s"
        with keystone_db_conn_dict.cursor() as cursor:
            cursor.execute(sql, project_name)
            return cursor.fetchone().get("id")

    def total_usage_for_project(
        usage_db_conn_dict, project_uuid, resource, start_time, end_time
    ):
        sql = "SELECT SUM(charge) AS sum FROM `usage` WHERE project_uuid = %s AND resource_type = %s AND start_time > %s AND end_time < %s"
        values = (project_uuid, resource, start_time, end_time)
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql, values)
            return cursor.fetchone().get("sum")

    def update_balance_for_project(project, total_usage):
        sql = 'INSERT INTO balances (project_record_id, used, last_updated) VALUES ("{record_id}", "{used}", "{last_updated}") ON DUPLICATE KEY UPDATE used="{used}", last_updated="{last_updated}"'
        # https://stackoverflow.com/questions/1136437/inserting-a-python-datetime-datetime-object-into-mysql
        last_updated = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%S")
        total_usage_int = round(total_usage)

        sql_with_vals = sql.format(
            record_id=project.get("id"),
            used=total_usage_int,
            last_updated=last_updated,
        )
        with usage_db_conn_dict.cursor() as cursor:
            cursor.execute(sql_with_vals)
        usage_db_conn_dict.commit()

    projects = get_projects()

    for project in projects:
        try:
            project_uuid = get_project_uuid(project.get("name"))
        except AttributeError:
            # No project UUID exists for this project name, move to next project
            continue
        total_usage = total_usage_for_project(
            usage_db_conn_dict,
            project_uuid,
            project.get("resource"),
            project.get("start_date"),
            project.get("end_date"),
        )
        if dry_run:
            print(
                "Project: {}, Resource: {}, Usage: {}".format(
                    project.get("name"), project.get("resource"), total_usage
                )
            )
        else:
            if total_usage is not None:
                update_balance_for_project(project, total_usage)
