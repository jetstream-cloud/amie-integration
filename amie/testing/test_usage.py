import os
import sys
import pytest

# This allows importing modules from parent directory
sys.path.insert(1, os.path.join(sys.path[0], "../.."))

from amie import usage
from amie import run_usage
from datetime import datetime, timedelta, timezone

instance_test_data = [
    {
        "note": "simple instance, created and then deleted a few minutes later",
        "uuid": "f74d1316-8f4c-4215-a4f3-9169b7d637bf",
        "non_resize_events_sql_result": [
            {"created_at": datetime(2022, 6, 16, 11, 59, 6), "action": "create"},
            {"created_at": datetime(2022, 6, 16, 12, 7, 11), "action": "delete"},
        ],
        "non_resize_events": [
            {"timestamp": datetime(2022, 6, 16, 11, 59, 6), "event": "create"},
            {"timestamp": datetime(2022, 6, 16, 12, 7, 11), "event": "delete"},
        ],
        "flavor_history": {"initial_flavor_id": 93, "resize_events": []},
        "combined_events": [
            {
                "timestamp": datetime(2021, 1, 1, 0, 0),
                "event": "billing_start",
            },
            {
                "timestamp": datetime(2022, 6, 16, 11, 59, 6),
                "event": "create",
                "new_flavor_id": 93,
            },
            {"timestamp": datetime(2022, 6, 16, 12, 7, 11), "event": "delete"},
            {
                "timestamp": datetime(2022, 8, 2, 23, 59, 59),
                "event": "billing_end",
            },
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 16, 11, 59, 6),
                "flavor_id": None,
                "start_timestamp": datetime(2021, 1, 1, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 16, 12, 7, 11),
                "flavor_id": 93,
                "start_timestamp": datetime(2022, 6, 16, 11, 59, 6),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 8, 2, 23, 59, 59),
                "flavor_id": 93,
                "start_timestamp": datetime(2022, 6, 16, 12, 7, 11),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 93,
                "start_timestamp": datetime(2022, 8, 2, 23, 59, 59),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2021, 1, 1, 0, 0, 0),
        "end_time": datetime(2022, 8, 2, 23, 59, 59),
        "usage": [
            {"charge": 4.311, "resource_type": "jetstream2-gpu.indiana.xsede.org"}
        ],
    },
    {
        "note": "instance from Junuh's tests with only create and delete",
        "uuid": "050be7c6-a375-4db5-b87f-162e9da83fbb",
        "non_resize_events_sql_result": [
            {"action": "create", "created_at": datetime(2022, 6, 16, 12, 9, 56)},
            {"action": "delete", "created_at": datetime(2022, 6, 18, 11, 54, 5)},
        ],
        "non_resize_events": [
            {"event": "create", "timestamp": datetime(2022, 6, 16, 12, 9, 56)},
            {"event": "delete", "timestamp": datetime(2022, 6, 18, 11, 54, 5)},
        ],
        "flavor_history": {"initial_flavor_id": 90, "resize_events": []},
        "combined_events": [
            {
                "event": "billing_start",
                "timestamp": datetime(2021, 1, 1, 0, 0),
            },
            {
                "event": "create",
                "new_flavor_id": 90,
                "timestamp": datetime(2022, 6, 16, 12, 9, 56),
            },
            {"event": "delete", "timestamp": datetime(2022, 6, 18, 11, 54, 5)},
            {"event": "billing_end", "timestamp": datetime(2022, 12, 1, 0, 0)},
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 16, 12, 9, 56),
                "flavor_id": None,
                "start_timestamp": datetime(2021, 1, 1, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 18, 11, 54, 5),
                "flavor_id": 90,
                "start_timestamp": datetime(2022, 6, 16, 12, 9, 56),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 12, 1, 0, 0),
                "flavor_id": 90,
                "start_timestamp": datetime(2022, 6, 18, 11, 54, 5),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 90,
                "start_timestamp": datetime(2022, 12, 1, 0, 0),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2021, 1, 1),
        "end_time": datetime(2022, 12, 1),
        "usage": [
            {"charge": 763.773, "resource_type": "jetstream2-gpu.indiana.xsede.org"}
        ],
    },
    {
        "note": "instance from Junuh's tests with a stop and start event",
        "uuid": "d144d0b0-d3d9-4372-ac53-4623a74501d9",
        "non_resize_events_sql_result": [
            {
                "action": "create",
                "created_at": datetime(2021, 12, 3, 1, 25, 11),
            },
            {"action": "stop", "created_at": datetime(2021, 12, 7, 15, 3, 14)},
            {
                "action": "start",
                "created_at": datetime(2021, 12, 7, 20, 10, 39),
            },
            {
                "action": "delete",
                "created_at": datetime(2022, 2, 1, 15, 11, 56),
            },
        ],
        "non_resize_events": [
            {"event": "create", "timestamp": datetime(2021, 12, 3, 1, 25, 11)},
            {"event": "stop", "timestamp": datetime(2021, 12, 7, 15, 3, 14)},
            {"event": "start", "timestamp": datetime(2021, 12, 7, 20, 10, 39)},
            {"event": "delete", "timestamp": datetime(2022, 2, 1, 15, 11, 56)},
        ],
        "flavor_history": {"initial_flavor_id": 36, "resize_events": []},
        "combined_events": [
            {"event": "billing_start", "timestamp": datetime(2021, 1, 1, 0, 0)},
            {
                "event": "create",
                "new_flavor_id": 36,
                "timestamp": datetime(2021, 12, 3, 1, 25, 11),
            },
            {"event": "stop", "timestamp": datetime(2021, 12, 7, 15, 3, 14)},
            {"event": "start", "timestamp": datetime(2021, 12, 7, 20, 10, 39)},
            {"event": "delete", "timestamp": datetime(2022, 2, 1, 15, 11, 56)},
            {"event": "billing_end", "timestamp": datetime(2022, 12, 1, 0, 0)},
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2021, 12, 3, 1, 25, 11),
                "flavor_id": None,
                "start_timestamp": datetime(2021, 1, 1, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2021, 12, 7, 15, 3, 14),
                "flavor_id": 36,
                "start_timestamp": datetime(2021, 12, 3, 1, 25, 11),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2021, 12, 7, 20, 10, 39),
                "flavor_id": 36,
                "start_timestamp": datetime(2021, 12, 7, 15, 3, 14),
                "state": "stopped",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 2, 1, 15, 11, 56),
                "flavor_id": 36,
                "start_timestamp": datetime(2021, 12, 7, 20, 10, 39),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 12, 1, 0, 0),
                "flavor_id": 36,
                "start_timestamp": datetime(2022, 2, 1, 15, 11, 56),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 36,
                "start_timestamp": datetime(2022, 12, 1, 0, 0),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2021, 1, 1),
        "end_time": datetime(2022, 12, 1),
        "usage": [
            {
                "charge": 185755.82200000001,
                "resource_type": "jetstream2.indiana.xsede.org",
            }
        ],
    },
    {
        "note": "instance with many shelve, unshelve, stop, and start events",
        "uuid": "9700814f-9465-422b-af51-78b31a2baa5f",
        "non_resize_events_sql_result": [
            {"action": "create", "created_at": datetime(2022, 3, 11, 15, 42, 2)},
            {"action": "stop", "created_at": datetime(2022, 3, 11, 16, 19, 15)},
            {"action": "shelve", "created_at": datetime(2022, 3, 11, 16, 19, 36)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 11, 18, 21, 48)},
            {"action": "stop", "created_at": datetime(2022, 3, 11, 18, 41, 4)},
            {"action": "shelve", "created_at": datetime(2022, 3, 11, 19, 2, 54)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 14, 12, 59, 40)},
            {"action": "shelve", "created_at": datetime(2022, 3, 14, 13, 1, 26)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 14, 19, 25, 11)},
            {"action": "stop", "created_at": datetime(2022, 3, 14, 19, 26, 18)},
            {"action": "start", "created_at": datetime(2022, 3, 14, 19, 28, 52)},
            {"action": "shelve", "created_at": datetime(2022, 3, 15, 10, 47, 52)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 16, 18, 57, 55)},
            {"action": "stop", "created_at": datetime(2022, 3, 16, 19, 9, 37)},
            {"action": "start", "created_at": datetime(2022, 3, 16, 19, 11, 38)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 9, 38, 48)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 9, 39, 22)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 9, 41, 41)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 9, 42, 5)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 9, 43, 42)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 9, 45, 55)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 9, 54, 15)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 9, 56, 22)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 10, 41, 7)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 10, 43, 3)},
            {"action": "shelve", "created_at": datetime(2022, 3, 17, 10, 44, 46)},
            {"action": "unshelve", "created_at": datetime(2022, 3, 17, 10, 47, 44)},
            {"action": "delete", "created_at": datetime(2022, 3, 18, 10, 31, 15)},
        ],
        "non_resize_events": [
            {"event": "create", "timestamp": datetime(2022, 3, 11, 15, 42, 2)},
            {"event": "stop", "timestamp": datetime(2022, 3, 11, 16, 19, 15)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 11, 16, 19, 36)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 11, 18, 21, 48)},
            {"event": "stop", "timestamp": datetime(2022, 3, 11, 18, 41, 4)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 11, 19, 2, 54)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 14, 12, 59, 40)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 14, 13, 1, 26)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 14, 19, 25, 11)},
            {"event": "stop", "timestamp": datetime(2022, 3, 14, 19, 26, 18)},
            {"event": "start", "timestamp": datetime(2022, 3, 14, 19, 28, 52)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 15, 10, 47, 52)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 16, 18, 57, 55)},
            {"event": "stop", "timestamp": datetime(2022, 3, 16, 19, 9, 37)},
            {"event": "start", "timestamp": datetime(2022, 3, 16, 19, 11, 38)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 38, 48)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 39, 22)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 41, 41)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 42, 5)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 43, 42)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 45, 55)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 54, 15)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 56, 22)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 10, 41, 7)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 10, 43, 3)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 10, 44, 46)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 10, 47, 44)},
            {"event": "delete", "timestamp": datetime(2022, 3, 18, 10, 31, 15)},
        ],
        "flavor_history": {"initial_flavor_id": 102, "resize_events": []},
        "combined_events": [
            {"event": "billing_start", "timestamp": datetime(2021, 1, 1, 0, 0)},
            {
                "event": "create",
                "new_flavor_id": 102,
                "timestamp": datetime(2022, 3, 11, 15, 42, 2),
            },
            {"event": "stop", "timestamp": datetime(2022, 3, 11, 16, 19, 15)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 11, 16, 19, 36)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 11, 18, 21, 48)},
            {"event": "stop", "timestamp": datetime(2022, 3, 11, 18, 41, 4)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 11, 19, 2, 54)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 14, 12, 59, 40)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 14, 13, 1, 26)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 14, 19, 25, 11)},
            {"event": "stop", "timestamp": datetime(2022, 3, 14, 19, 26, 18)},
            {"event": "start", "timestamp": datetime(2022, 3, 14, 19, 28, 52)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 15, 10, 47, 52)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 16, 18, 57, 55)},
            {"event": "stop", "timestamp": datetime(2022, 3, 16, 19, 9, 37)},
            {"event": "start", "timestamp": datetime(2022, 3, 16, 19, 11, 38)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 38, 48)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 39, 22)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 41, 41)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 42, 5)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 43, 42)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 45, 55)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 9, 54, 15)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 9, 56, 22)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 10, 41, 7)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 10, 43, 3)},
            {"event": "shelve", "timestamp": datetime(2022, 3, 17, 10, 44, 46)},
            {"event": "unshelve", "timestamp": datetime(2022, 3, 17, 10, 47, 44)},
            {"event": "delete", "timestamp": datetime(2022, 3, 18, 10, 31, 15)},
            {"event": "billing_end", "timestamp": datetime(2022, 12, 1, 0, 0)},
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 15, 42, 2),
                "flavor_id": None,
                "start_timestamp": datetime(2021, 1, 1, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 16, 19, 15),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 15, 42, 2),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 16, 19, 36),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 16, 19, 15),
                "state": "stopped",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 18, 21, 48),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 16, 19, 36),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 18, 41, 4),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 18, 21, 48),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 11, 19, 2, 54),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 18, 41, 4),
                "state": "stopped",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 14, 12, 59, 40),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 11, 19, 2, 54),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 14, 13, 1, 26),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 14, 12, 59, 40),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 14, 19, 25, 11),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 14, 13, 1, 26),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 14, 19, 26, 18),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 14, 19, 25, 11),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 14, 19, 28, 52),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 14, 19, 26, 18),
                "state": "stopped",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 15, 10, 47, 52),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 14, 19, 28, 52),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 16, 18, 57, 55),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 15, 10, 47, 52),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 16, 19, 9, 37),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 16, 18, 57, 55),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 16, 19, 11, 38),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 16, 19, 9, 37),
                "state": "stopped",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 38, 48),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 16, 19, 11, 38),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 39, 22),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 38, 48),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 41, 41),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 39, 22),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 42, 5),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 41, 41),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 43, 42),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 42, 5),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 45, 55),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 43, 42),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 54, 15),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 45, 55),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 9, 56, 22),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 54, 15),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 10, 41, 7),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 9, 56, 22),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 10, 43, 3),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 10, 41, 7),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 10, 44, 46),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 10, 43, 3),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 17, 10, 47, 44),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 10, 44, 46),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 3, 18, 10, 31, 15),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 17, 10, 47, 44),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 12, 1, 0, 0),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 3, 18, 10, 31, 15),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 12, 1, 0, 0),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2021, 1, 1),
        "end_time": datetime(2022, 12, 1),
        "usage": [
            {
                "charge": 7152.782999999999,
                "resource_type": "jetstream2-gpu.indiana.xsede.org",
            }
        ],
    },
    {
        "note": "instance with resize between flavor types",
        "uuid": "c83ba63c-ec13-4acc-b8df-b245c67aeb09",
        "non_resize_events_sql_result": [
            {"action": "create", "created_at": datetime(2022, 6, 19, 1, 25, 23)},
            {"action": "shelve", "created_at": datetime(2022, 6, 19, 3, 10, 30)},
            {"action": "unshelve", "created_at": datetime(2022, 6, 19, 5, 20, 14)},
            {"action": "shelve", "created_at": datetime(2022, 6, 19, 14, 2)},
            {"action": "unshelve", "created_at": datetime(2022, 6, 21, 18, 53, 54)},
            {"action": "delete", "created_at": datetime(2022, 6, 21, 18, 54, 28)},
        ],
        "non_resize_events": [
            {"event": "create", "timestamp": datetime(2022, 6, 19, 1, 25, 23)},
            {"event": "shelve", "timestamp": datetime(2022, 6, 19, 3, 10, 30)},
            {"event": "unshelve", "timestamp": datetime(2022, 6, 19, 5, 20, 14)},
            {"event": "shelve", "timestamp": datetime(2022, 6, 19, 14, 2)},
            {"event": "unshelve", "timestamp": datetime(2022, 6, 21, 18, 53, 54)},
            {"event": "delete", "timestamp": datetime(2022, 6, 21, 18, 54, 28)},
        ],
        "flavor_history": {
            "initial_flavor_id": 30,
            "resize_events": [
                {
                    "event": "resize",
                    "new_flavor_id": 102,
                    "old_flavor_id": 30,
                    "timestamp": datetime(2022, 6, 19, 2, 24, 36),
                }
            ],
        },
        "combined_events": [
            {"event": "billing_start", "timestamp": datetime(2021, 1, 1, 0, 0)},
            {
                "event": "create",
                "new_flavor_id": 30,
                "timestamp": datetime(2022, 6, 19, 1, 25, 23),
            },
            {
                "event": "resize",
                "new_flavor_id": 102,
                "old_flavor_id": 30,
                "timestamp": datetime(2022, 6, 19, 2, 24, 36),
            },
            {"event": "shelve", "timestamp": datetime(2022, 6, 19, 3, 10, 30)},
            {"event": "unshelve", "timestamp": datetime(2022, 6, 19, 5, 20, 14)},
            {"event": "shelve", "timestamp": datetime(2022, 6, 19, 14, 2)},
            {"event": "unshelve", "timestamp": datetime(2022, 6, 21, 18, 53, 54)},
            {"event": "delete", "timestamp": datetime(2022, 6, 21, 18, 54, 28)},
            {"event": "billing_end", "timestamp": datetime(2022, 8, 10, 23, 59, 59)},
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 19, 1, 25, 23),
                "flavor_id": None,
                "start_timestamp": datetime(2021, 1, 1, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 19, 2, 24, 36),
                "flavor_id": 30,
                "start_timestamp": datetime(2022, 6, 19, 1, 25, 23),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 19, 3, 10, 30),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 19, 2, 24, 36),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 19, 5, 20, 14),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 19, 3, 10, 30),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 19, 14, 2),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 19, 5, 20, 14),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 21, 18, 53, 54),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 19, 14, 2),
                "state": "shelved_offloaded",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 6, 21, 18, 54, 28),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 21, 18, 53, 54),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 8, 10, 23, 59, 59),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 6, 21, 18, 54, 28),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 8, 10, 23, 59, 59),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2021, 1, 1, 0, 0, 0),
        "end_time": datetime(2022, 8, 10, 23, 59, 59),
        "usage": [
            {"charge": 63.164, "resource_type": "jetstream2.indiana.xsede.org"},
            {
                "charge": 1212.2310000000002,
                "resource_type": "jetstream2-gpu.indiana.xsede.org",
            },
        ],
    },
    {
        "note": "instance that was deleted but has no delete event in instance_actions table",
        "uuid": "0f86b778-5b98-47a9-bf0d-f78fb9c9e992",
        "non_resize_events_sql_result": [
            {"action": "create", "created_at": datetime(2022, 10, 9, 4, 40, 14)}
        ],
        "non_resize_events": [
            {"event": "create", "timestamp": datetime(2022, 10, 9, 4, 40, 14)}
        ],
        "flavor_history": {"initial_flavor_id": 102, "resize_events": []},
        "combined_events": [
            {
                "event": "billing_start",
                "timestamp": datetime(2022, 10, 9, 0, 0),
            },
            {
                "event": "create",
                "new_flavor_id": 102,
                "timestamp": datetime(2022, 10, 9, 4, 40, 14),
            },
            {"event": "delete", "timestamp": datetime(2022, 10, 9, 4, 48, 23)},
            {
                "event": "billing_end",
                "timestamp": datetime(2022, 10, 9, 23, 59, 59),
            },
        ],
        "state_intervals": [
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 10, 9, 4, 40, 14),
                "flavor_id": None,
                "start_timestamp": datetime(2022, 10, 9, 0, 0),
                "state": "not_yet_created",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 10, 9, 4, 48, 23),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 10, 9, 4, 40, 14),
                "state": "active",
            },
            {
                "billing_active": True,
                "end_timestamp": datetime(2022, 10, 9, 23, 59, 59),
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 10, 9, 4, 48, 23),
                "state": "deleted",
            },
            {
                "billing_active": False,
                "end_timestamp": None,
                "flavor_id": 102,
                "start_timestamp": datetime(2022, 10, 9, 23, 59, 59),
                "state": "deleted",
            },
        ],
        "start_time": datetime(2022, 10, 9, 0, 0, 0),
        "end_time": datetime(2022, 10, 9, 23, 59, 59),
        "usage": [
            {"charge": 17.387, "resource_type": "jetstream2-gpu.indiana.xsede.org"}
        ],
    },
]

project_test_data = [
    {"id": "af24aba824414bbc94e33ea86c757e0a"},
    {"id": "515af3f0d6da445a9919f571743794ef"},
    {"id": "7f6a56f00f344d1ab5397c43d72a41c7"},
]

flavor_test_data = [
    {"id": 15, "multiplier": 8, "resource_type": "jetstream2.indiana.xsede.org"},
    {"id": 102, "multiplier": 128, "resource_type": "jetstream2-gpu.indiana.xsede.org"},
]


@pytest.fixture(params=instance_test_data)
def instance_fixture(request):
    return request.param


@pytest.fixture(params=project_test_data)
def project_fixture(request):
    return request.param


@pytest.fixture(params=flavor_test_data)
def flavor_fixture(request):
    return request.param


nova_db_conn_dict = run_usage.nova_db_conn_dict()
nova_api_db_conn_dict = run_usage.nova_api_db_conn_dict()
keystone_db_conn_dict = run_usage.keystone_db_conn_dict()


def test_instance_non_resize_events_sql_result(instance_fixture):
    assert instance_fixture.get(
        "non_resize_events_sql_result"
    ) == usage.get_non_resize_events_for_instance_sql_query_(
        nova_db_conn_dict, instance_fixture.get("uuid")
    )


def test_instance_non_resize_events_query_results_to_events(instance_fixture):
    assert instance_fixture.get(
        "non_resize_events"
    ) == usage.get_non_resize_events_for_instance_query_results_to_events_(
        instance_fixture.get("non_resize_events_sql_result")
    )


def test_get_flavor_history_for_instance(instance_fixture):
    assert instance_fixture.get(
        "flavor_history"
    ) == usage.get_flavor_history_for_instance(
        nova_db_conn_dict, instance_fixture.get("uuid")
    )


def test_combined_events_for_instance(instance_fixture):
    assert instance_fixture.get(
        "combined_events"
    ) == usage.combined_events_for_instance(
        nova_db_conn_dict,
        instance_fixture.get("uuid"),
        instance_fixture.get("start_time"),
        instance_fixture.get("end_time"),
    )


def test_get_state_intervals_from_events(instance_fixture):
    assert instance_fixture.get(
        "state_intervals"
    ) == usage.get_state_intervals_from_events(instance_fixture.get("combined_events"))


def test_get_total_charges_for_instance(instance_fixture):
    assert instance_fixture.get("usage") == usage.get_total_charges_for_instance(
        nova_db_conn_dict,
        nova_api_db_conn_dict,
        instance_fixture.get("uuid"),
        instance_fixture.get("start_time"),
        instance_fixture.get("end_time"),
    )


def test_get_projects_in_xsede_domain(project_fixture):
    project_ids = usage.get_project_ids_in_xsede_domain(keystone_db_conn_dict)
    assert project_fixture.get("id") in project_ids


def test_flavor_charge_info(flavor_fixture):
    flavor_charge_info = usage.get_flavor_charge_info(
        nova_api_db_conn_dict, flavor_fixture.get("id")
    )
    assert flavor_charge_info.items() <= flavor_fixture.items()


def test_billing_start_event():
    assert usage.get_billing_start_event(None) == {
        "timestamp": datetime.fromtimestamp(0),
        "event": "billing_start",
    }
    assert usage.get_billing_start_event(datetime(2021, 1, 1)) == {
        "timestamp": datetime(2021, 1, 1, 0, 0),
        "event": "billing_start",
    }


def test_billing_end_event():
    noneEvent = usage.get_billing_end_event(None)
    assert noneEvent.get("timestamp") - datetime.now() < timedelta(seconds=1)
    assert noneEvent.get("event") == "billing_end"
    assert usage.get_billing_end_event(datetime(2021, 1, 1)) == {
        "timestamp": datetime(2021, 1, 1, 0, 0),
        "event": "billing_end",
    }
