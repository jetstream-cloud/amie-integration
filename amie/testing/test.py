# from unittest.mock import Mock
import unittest.mock as mock
import pytest
import sys, os
from datetime import date, datetime
from amieclient import AMIEClient

# sys.path.append(
#    os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir)))

from amie import packet_processor as packet_processor
from amie import util as util


@pytest.fixture()
def mock_packet():
    return mock.Mock()


def test_create_itc_reply(mock_packet):
    p = packet_processor.PacketProcessor(True)
    p.create_itc(mock_packet)
    mock_packet.reply_packet.assert_called()


@mock.patch("amie.packet_processor.AMIEClient")
def test_process_dpc_dac_send_itc(mock_AMIEClient):
    mock_packet = mock.Mock()
    p = packet_processor.PacketProcessor(True)
    p.process_dpc_dac(mock_packet)
    p.amie_client.send_packet.assert_called()


# @mock.patch('packet_processor.AMIEClient')
# def test_process_rum_send_itc(mock_AMIEClient):
#    mock_packet = mock.Mock()
#    p = packet_processor.PacketProcessor(True)
#    p.process_rum(mock_packet)
#    p.amie_client.send_packet.assert_called()


@mock.patch("amie.util.client.Client")
def test_supplement_project(mock_keystoneclient):
    u = util.Util(testing=True)
    project_record = util.model.Project(
        grant_id="TEST-1234",
        resource="test_resource_cpu",
        start_date=date(2021, 1, 1),
        end_date=date(2021, 12, 31),
        award=10000.0,
        balance=5000.0,
    )
    u.session.add(project_record)
    u.session.commit()
    u.supplement_project("TEST-1234", "test_resource_cpu", 5000.0)
    p = (
        u.session.query(util.model.Project)
        .filter(util.model.Project.grant_id == "TEST-1234")
        .one()
    )
    assert p.award == 15000.0
    assert p.balance == 5000.0
    assert p.start_date == date(2021, 1, 1)
    assert p.end_date == date(2021, 12, 31)
    u.supplement_project("TEST-1234", "test_resource_cpu", -5000.0)
    p = (
        u.session.query(util.model.Project)
        .filter(util.model.Project.grant_id == "TEST-1234")
        .one()
    )
    assert p.award == 10000.0
    assert p.balance == 5000.0
    assert p.start_date == date(2021, 1, 1)
    assert p.end_date == date(2021, 12, 31)


def test_update_email():
    u = util.Util(testing=True)
    u.keystone.users.update = mock.Mock()
    u.update_email("1", email="email@example.com")
    u.keystone.users.update.assert_called_with("1", email="email@example.com")


@mock.patch("amie.packet_processor.AMIEClient")
def test_rum_packet(mock_AMIEClient):
    from amieclient.packet import RequestUserModify

    rum = RequestUserModify()
    rum.Email = "email@example.com"
    rum.person_id = "1"
    rum.Actiontype = "replace"
    p = packet_processor.PacketProcessor(True)
    p.u.update_email = mock.Mock()
    p.process_rum(rum)
    p.u.update_email.assert_called_with("1", "email@example.com")
    p.amie_client.send_packet.assert_called()


@mock.patch("amie.packet_processor.AMIEClient")
def test_process_packet_types(mock_AMIEClient):
    from amieclient.packet import (
        RequestUserModify,
        RequestProjectCreate,
        RequestAccountCreate,
        DataProjectCreate,
        DataAccountCreate,
        RequestPersonMerge,
        RequestProjectInactivate,
        RequestAccountInactivate,
        RequestProjectReactivate,
    )

    p = packet_processor.PacketProcessor(True)
    p.process_rpc = mock.Mock()
    p.process_rac = mock.Mock()
    p.process_dpc_dac = mock.Mock()
    p.process_rum = mock.Mock()
    p.process_rpm = mock.Mock()
    p.process_rpi = mock.Mock()
    p.process_rai = mock.Mock()
    p.process_rpr = mock.Mock()
    packet = RequestProjectCreate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rpc.assert_called()
    packet = RequestAccountCreate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rac.assert_called()
    packet = DataProjectCreate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_dpc_dac.assert_called()
    packet = DataAccountCreate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_dpc_dac.assert_called()
    packet = RequestUserModify()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rum.assert_called()
    packet = RequestProjectInactivate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rpi.assert_called()
    packet = RequestAccountInactivate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rai.assert_called()
    packet = RequestProjectReactivate()
    packet.packet_timestamp = datetime.now()
    p.process_packets(
        [
            packet,
        ]
    )
    p.process_rpr.assert_called()


@mock.patch("amie.packet_processor.AMIEClient")
def test_process_rai(mock_AMIECLIENT):
    from amieclient.packet import RequestAccountInactivate

    packet = RequestAccountInactivate()
    packet.ResourceList = ["testresource1", "testresource2"]
    packet.ProjectID = "testproject"
    packet.PersonID = "testpersonid001"
    p = packet_processor.PacketProcessor(True)
    p.u.user_group_remove = mock.Mock()
    p.process_rai(packet)
    p.u.user_group_remove.assert_called_with("testproject", "testpersonid001")


@mock.patch("amie.packet_processor.AMIEClient")
def test_process_rpr_rpi(moc_AMIECLIENT):
    from amieclient.packet import RequestProjectInactivate, RequestProjectReactivate

    rpi = RequestProjectInactivate()
    rpr = RequestProjectReactivate()
    rpi.ProjectID = "testproject"
    rpr.ProjectID = "testproject"
    p = packet_processor.PacketProcessor(True)
    p.u.disable_project = mock.Mock()
    p.u.enable_project = mock.Mock()
    p.process_rpi(rpi)
    p.u.disable_project.assert_called_with("testproject")
    p.process_rpr(rpr)
    p.u.enable_project.assert_called_with("testproject")


@mock.patch("amie.packet_processor.AMIEClient")
def test_process_rpc(mock_AMIEClient):
    from amieclient.packet import RequestProjectCreate

    p = packet_processor.PacketProcessor(True)
    packet = RequestProjectCreate()
    packet.GrantNumber = "testgrant001"
    packet.ResourceList = ["testresource1", "testresource2"]
    packet.AllocationType = "new"
    packet.SitePersonId = [{"PersonID": "jdoe", "Site": "X-PORTAL"}]
    packet.PiRequestedLoginList = [
        "testuser",
    ]

    p.u.create_openstack_account = mock.Mock()
    p.u.user_group_add = mock.Mock()
    p.u.create_project = mock.Mock()
    p.process_rpc(packet)
    p.u.create_project.assert_called()
    p.u.user_group_add.assert_called()
    p.u.create_openstack_account.assert_called()

    packet.AllocationType = "renewal"
    p.u.renew_project = mock.Mock()
    p.process_rpc(packet)
    p.u.renew_project.assert_called()

    packet.AllocationType = "supplement"
    p.u.supplement_project = mock.Mock()
    p.process_rpc(packet)
    p.u.supplement_project.assert_called()

    packet.AllocationType = "transfer"
    p.process_rpc(packet)

    packet.AllocationType = "adjustment"
    p.u.adjust_project = mock.Mock()
    p.process_rpc(packet)
    p.u.adjust_project.assert_called()

    packet.AllocationType = "extension"
    p.u.extend_project = mock.Mock()
    p.process_rpc(packet)
    p.u.extend_project.assert_called()


# test cases
# check multiple requested resources for a project create make multiple allocation records
# check requested login is stored
# check already allocated login/uid does not create user or storage for rpc and rac
# check rpc makes project and group
# check rpc puts pi in project group
# check rpi disables project
# check rpr enables project
