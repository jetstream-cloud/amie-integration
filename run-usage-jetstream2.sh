#!/bin/bash

source /home/amie/amie-integration-venv/bin/activate
source /home/amie/amie-integration/amierc

ONE_DAY_OF_SECONDS=$((60*60*24))

EPOCH_NOW=$(date -u "+%s")
EPOCH_24H_AGO=$(($EPOCH_NOW-$ONE_DAY_OF_SECONDS))

YESTERDAY_START=$(date -u --date @$EPOCH_24H_AGO "+%Y-%m-%d")
TODAY_START=$(date -u "+%Y-%m-%d")

export USAGE_DB_URL="${AMIE_USAGE_DB_URL}"
export KEYSTONE_DOMAIN_ID="${AMIE_KEYSTONE_DOMAIN_ID}"

python3 /home/amie/amie-integration/amie/run_usage.py generate-records --start $YESTERDAY_START --end $TODAY_START --write-to-db
python3 /home/amie/amie-integration/amie/run_usage.py update-balances
python3 /home/amie/amie-integration/amie/run_usage.py upload-records
python3 /home/amie/amie-integration/amie/run_usage.py reupload-failed-records

export USAGE_DB_URL="${NAIRR_USAGE_DB_URL}"
export KEYSTONE_DOMAIN_ID="${NAIRR_KEYSTONE_DOMAIN_ID}"

python3 /home/amie/amie-integration/amie/run_usage.py generate-records --start $YESTERDAY_START --end $TODAY_START --write-to-db
