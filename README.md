# Jetstream2 AMIE integration

## Getting Started Contributing

Run these commands to set up a Python virtual environment and install the project dependencies.

```
virtualenv ../amie-integration-venv
source ../amie-integration-venv/bin/activate
pip install -r requirements.txt
```

## Setting up amierc

amierc file sets environment variables that contain credentials for connecting to OpenStack databases and AMIE API. You need to set these variables before running the usage reporting code. Suggestes steps to do that:

- Copy `amierc-example` to `amierc`
- Edit `amierc`, replace the placeholder values with your actual values
- Run `export amierc` to set the variables in your shell

## How to run usage reporting code

An example:

```
source amierc
python3 amie/run_usage.py usage-for-instance --start 2022-08-01 --end 2022-08-02 f35cc589-ebd4-4025-83d4-9d8491b43513
```

Run `python3 amie/run_usage.py` for help on using the various subcommands.


### Code Formatting

This project uses [black](https://github.com/psf/black) to format code. After running the above commands, simply run `black your-changed-file-here` to apply formatting, or `black .` at the root of the project to format everything.

If you use an IDE, consider configuring it to format code every time you save a file. This makes life easier! [Here is how](https://black.readthedocs.io/en/stable/integrations/editors.html).

## How to run tests

Activate your virtual environment, then:

```
source amierc
cd amie
python -m pytest
```

---

## How to hand-calculate usage charges

In order to write confident test fixtures, we need to hand-calculate the usage charges for instances that we write tests for.


### Hand-calculation: simple case

Let's figure out the usage for the simplest case, instance with UUID `f74d1316-8f4c-4215-a4f3-9169b7d637bf`. We need to run four SQL queries to gather some information.

#### Gathering Information

First query, we look up non-resize events for the instance.

```
mysql> SELECT created_at, action FROM nova.instance_actions WHERE instance_uuid='f74d1316-8f4c-4215-a4f3-9169b7d637bf';
+---------------------+--------+
| created_at          | action |
+---------------------+--------+
| 2022-06-16 11:59:06 | create |
| 2022-06-16 12:07:11 | delete |
+---------------------+--------+
2 rows in set (0.00 sec)
```

So, this instance was created at `2022-06-16 11:59:06` and deleted at `2022-06-16 12:07:11`, that's 8 minutes and 5 seconds. We see no other non-resize events relevant to billing. Nice and simple. (By the way, all these times are in UTC.)

Second query, we look for any resize events for the instance:

```
mysql> SELECT created_at, updated_at, status, old_instance_type_id, new_instance_type_id FROM nova.migrations WHERE migration_type = 'resize' AND instance_uuid='f74d1316-8f4c-4215-a4f3-9169b7d637bf';
Empty set (0.00 sec)
```

No resize events for this one. That means the most recent flavor is the only flavor relevant to billing.

Third query, we learn the most recent flavor ID this way:

```
mysql> SELECT instance_type_id FROM nova.instances WHERE uuid = 'f74d1316-8f4c-4215-a4f3-9169b7d637bf';

+------------------+
| instance_type_id |
+------------------+
|               93 |
+------------------+
1 row in set (0.01 sec)
```

Fourth and final query, we look that flavor up, note the different database:

```
mysql> SELECT name, vcpus FROM nova_api.flavors WHERE id = 93;
+-----------+-------+
| name      | vcpus |
+-----------+-------+
| g3.medium |     8 |
+-----------+-------+
1 row in set (0.00 sec)
```

(Note that the `id` field in the `nova_api.flavors` table is _not_ the same thing as the `flavorid` field! `flavorid` is what appears as the "ID" field in the OpenStack API/CLI, e.g., when you do `openstack flavor list`. But `id` is what the database uses internally e.g. in the `nova.migrations` and `nova.instances` tables. So we need to use that `id` here. Welcome to OpenStack.)

Now we know everything needed to calculate the usage charge for the instance.

#### Calculating usage

Subtracting the creation time from the deletion time, we see the instance was active for 8 minutes and 5 seconds. Converting this to hours, we get 0.135 hours.

We figure out the flavor charge multiplier from the `flavor_type_charge_multipliers` dict in `usage.py`. In this case, the instance was a `g3` flavor, so we multiply the number of vCPUs by 4 to get the total SU charge _per hour_. 4 times 8 vCPUs is 32, so this flavor burns 32 SUs per hour.

Finally, we multiply the time the instance was active (0.135 hours) by the flavor charge multiplier (32) to get **4.32** SUs consumed, all of the GPU resource type.

### Hand-calculation: complex case

Now let's look at instance `c83ba63c-ec13-4acc-b8df-b245c67aeb09`. We run the same SQL queries as the simple case above. Note that we select from `nova_api.flavors` multiple times, for each flavor `id` that we see the instance used.

#### Gathering information

```
mysql> SELECT created_at, action FROM nova.instance_actions WHERE instance_uuid='c83ba63c-ec13-4acc-b8df-b245c67aeb09';
+---------------------+---------------+
| created_at          | action        |
+---------------------+---------------+
| 2022-06-19 01:25:23 | create        |
| 2022-06-19 02:24:35 | resize        |
| 2022-06-19 02:30:18 | confirmResize |
| 2022-06-19 02:54:48 | reboot        |
| 2022-06-19 03:10:30 | shelve        |
| 2022-06-19 05:20:14 | unshelve      |
| 2022-06-19 14:02:00 | shelve        |
| 2022-06-21 18:53:54 | unshelve      |
| 2022-06-21 18:54:28 | delete        |
+---------------------+---------------+
9 rows in set (0.00 sec)

mysql> SELECT created_at, updated_at, status, old_instance_type_id, new_instance_type_id FROM nova.migrations WHERE migration_type = 'resize' AND instance_uuid='c83ba63c-ec13-4acc-b8df-b245c67aeb09';
+---------------------+---------------------+-----------+----------------------+----------------------+
| created_at          | updated_at          | status    | old_instance_type_id | new_instance_type_id |
+---------------------+---------------------+-----------+----------------------+----------------------+
| 2022-06-19 02:24:36 | 2022-06-19 02:30:20 | confirmed |                   30 |                  102 |
+---------------------+---------------------+-----------+----------------------+----------------------+
1 row in set (0.00 sec)

mysql> SELECT instance_type_id FROM nova.instances WHERE uuid = 'c83ba63c-ec13-4acc-b8df-b245c67aeb09';
+------------------+
| instance_type_id |
+------------------+
|              102 |
+------------------+
1 row in set (0.00 sec)

mysql> SELECT name, vcpus FROM nova_api.flavors WHERE id = 30;
+--------+-------+
| name   | vcpus |
+--------+-------+
| m3.2xl |    64 |
+--------+-------+
1 row in set (0.00 sec)

mysql> SELECT name, vcpus FROM nova_api.flavors WHERE id = 102;
+-------+-------+
| name  | vcpus |
+-------+-------+
| g3.xl |    32 |
+-------+-------+
1 row in set (0.00 sec)
```

#### Calculating usage

This instance had a more complicated life! We can put the events together to obtain this story:

- Created at `2022-06-19 01:25:23` with flavor `m3.2xl`
  - (per `old_instance_type_id` field in `nova.migrations` table)
- Resized at `2022-06-19 02:24:36` to flavor `g3.xl`
  - (per `new_instance_type_id` in `nova.migrations`, and also `instance_type_id` in `nova.instances` because it was not resized again afterward)
- Shelved at `2022-06-19 03:10:30`
- Unshelved at `2022-06-19 05:20:14`
- Shelved again at `2022-06-19 14:02:00`
- Unshelved again at `2022-06-21 18:53:54`
- Deleted at `2022-06-21 18:54:28`

To obtain charges, we need to think in terms of "state intervals" instead of events, so we can transform the event history into these intervals:

- From `2022-06-19 01:25:23` to `2022-06-19 02:24:36`, active with flavor `m3.2xl`
- From `2022-06-19 02:24:36` to `2022-06-19 03:10:30`, active with flavor `g3.xl`
- From `2022-06-19 03:10:30` to `2022-06-19 05:20:14`, shelved
- From `2022-06-19 05:20:14` to `2022-06-19 14:02:00`, active with flavor `g3.xl`
- From `2022-06-19 14:02:00` to `2022-06-21 18:53:54`, shelved
- From `2022-06-21 18:53:54` to `2022-06-21 18:54:28`, active with flavor `g3.xl`

For each state interval, figure out the number of elapsed hours:

- 0.9869 hours, active with flavor `m3.2xl`
- 0.765 hours, active with flavor `g3.xl`
- 2.1622 hours, shelved
- 8.6961 hours, active with flavor `g3.xl`
- 52.865 hours, shelved
- 0.0094 hours, active with flavor `g3.xl`

Then, use the same technique as the simple case above to determine the flavor type and vCPU multipliers. We multiply the hours in each state interval by both of these multipliers to obtain the SU charge for each state interval.

- 0.9869 hours x 1 (active state) x 1 (flavor type multiplier) x 64 (vCPUs) = 63.162 SUs of default resource type
- 0.765 hours x 1 (active state) x 4 (flavor type multiplier) x 32 (vCPUs) = 97.92 SUs of GPU resource
- 2.1622 hours x 0 (shelved state) = 0 SUs
  - (the flavor doesn't matter because shelved state results in zero charge)
- 8.6961 hours x 1 (active state) x 4 (flavor type multiplier) x 32 (vCPUs) = 1113.101 SUs of GPU resource
- 52.865 hours x 0 (shelved state) = 0 SUs
- 0.0094 hours x 1 (active state) x 4 (flavor type multiplier) x 32 (vCPUs) = 1.203 SUs of GPU resource

Finally, sum the SU charge for each resource type across state intervals to obtain **total charges of 63.162 SUs of the default resource, and 1212.224 SUs of the GPU resource**.

## Miscellaneous Tricks

## Recovering from a gap in usage reporting

There may be a Jetstream2 site outage causing usage reporting to not run on its usual schedule for one or more days. (e.g. because the usage reporting code could not run at all, could not talk to the DB or control plane, etc.) In this case, once services are recovered, you may wish to do a few things.

You may wish to report usage during certain days that were missed by the automated ([Rundeck](https://staff.jetstream-cloud.org/books/misc/page/rundeck-evaluation-notes)) execution. SSH to `j2m1`, `su` to `amie` user, and run parts of `/home/amie/amie-integration/run-usage-jetstream2.sh` interactively to report usage for the missing days. For example, to report usage for the UTC day 2024-04-24:

```
amie@j2m1:~/amie-integration$ source /home/amie/amie-integration-venv/bin/activate
(amie-integration-venv) amie@j2m1:~/amie-integration$ source /home/amie/amie-integration/amierc
# Repeat the next command as needed, with the dates adjusted each time, to record usage for multiple days. Do not run it for more than a single day at a time. `--start` and `--end` should be one day apart (the script assumes midnight UTC).
(amie-integration-venv) amie@j2m1:~/amie-integration$ python3 /home/amie/amie-integration/amie/run_usage.py generate-records --start 2024-04-24 --end 2024-04-25 --write-to-db
# The next two commands may take a long time to run
(amie-integration-venv) amie@j2m1:~/amie-integration$ python3 /home/amie/amie-integration/amie/run_usage.py update-balances
(amie-integration-venv) amie@j2m1:~/amie-integration$ time python3 /home/amie/amie-integration/amie/run_usage.py upload-records

```

You may wish to skip reporting for days that we don't wish to charge anyone for usage, because (e.g.) the cloud was offline. Simply don't run usage reporting for these days.

You may wish to mass-issue allocation credits to users for any usage that we reported while Jetstream2 wasn't actually working. We cannot do this directly. Contact [ACCESS Allocations support](https://support.access-ci.org/help-ticket) for assistance.

See <https://gitlab.com/jetstream-cloud/project-mgt/-/issues/182> for more details of how we handled this situation for the mid-April 2024 outage.

### Delete All Error Records From Usage API

```
cmart@thinkpad:~$ curl -s -H "XA-SITE: IU" -H "XA-API-KEY: redacted" https://usage.xsede.org/api/v1_test/usage/failed | jq -r '.FailedRecords | .[] | .FailedRecordID' > /tmp/failed-record-ids
cmart@thinkpad:~$ lines=$(cat /tmp/failed-record-ids)
cmart@thinkpad:~$ for line in $lines
> do
> curl -X DELETE -H "XA-SITE: IU" -H "XA-API-KEY: redacted" https://usage.xsede.org/api/v1_test/usage/failed/$line
> done
```

This may take a few minutes if there are a lot of failed records.
