import sqlalchemy
import smtplib
import csv
import os
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email import encoders
from datetime import datetime

db_url = os.getenv("NAIRR_USAGE_DB_URL")
smtp_host = os.getenv("SMTP_HOST")
smtp_user = os.getenv("SMTP_USERNAME")
smtp_pass = os.getenv("SMTP_PASSWORD")
smtp_from = os.getenv("SMTP_FROM_ADDRESS")
recipient_email = os.getenv("RECIPIENT_ADDRESS")

if not all([db_url, smtp_host, smtp_user, smtp_pass, smtp_from, recipient_email]):
    raise ValueError("Missing required environment variables")

# Generate filename with timestamp
filename = f"nairr_usage_report.csv"

try:
    # Create engine - replace mysql:// with mysql+pymysql:// in the URL
    engine = sqlalchemy.create_engine(db_url.replace("mysql://", "mysql+pymysql://"))

    query = """
    SELECT 
        kp.name AS "grant_number",
        klu.name AS "username",
        resource_type AS "resource",
        SUM(nu.charge) AS "charge"
    FROM nairr.usage nu
    LEFT JOIN keystone.project kp 
        ON nu.project_uuid = kp.id
    LEFT JOIN keystone.local_user klu 
        ON nu.user_uuid = klu.user_id
    GROUP BY 
        kp.name,
        klu.name,
        resource;
    """

    with engine.connect() as connection:
        result = connection.execute(sqlalchemy.text(query))

        # Write results to CSV
        with open(filename, "w", newline="") as csvfile:
            csvwriter = csv.writer(csvfile)
            # Write headers
            csvwriter.writerow(result.keys())
            # Write data rows
            csvwriter.writerows(result.fetchall())

    # Create email
    msg = MIMEMultipart()
    msg["From"] = smtp_from
    msg["To"] = recipient_email
    msg["Subject"] = "NAIRR Usage Report"

    # Add body text
    body = "NAIRR usage report attached."
    msg.attach(MIMEText(body, "plain"))

    # Add attachment
    with open(filename, "rb") as attachment:
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())

    encoders.encode_base64(part)
    part.add_header("Content-Disposition", f"attachment; filename= {filename}")
    msg.attach(part)

    # Send email
    with smtplib.SMTP(smtp_host) as server:
        server.starttls()
        server.login(smtp_user, smtp_pass)
        server.send_message(msg)

    print(f"Report sent successfully to {recipient_email}")

except Exception as e:
    print(f"Error: {str(e)}")
    raise

finally:
    if os.path.exists(filename):
        os.remove(filename)
